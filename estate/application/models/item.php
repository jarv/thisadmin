<?php 
class Item extends Base_Model
{
	protected $table_name;
	
	function __construct()
	{
		parent::__construct();
		$this->table_name = 'ec_items';
	}
	
	function exists($data)
	{
		$this->db->from($this->table_name);
		
		if (isset($data['id'])) {
			$this->db->where('id', $data['id']);
		}
		
		if (isset($data['name'])) {
			$this->db->where('name', $data['name']);
		}
		
		if (isset($data['city_id'])) {
			$this->db->where('city_id', $data['city_id']);
		}
		
		$query = $this->db->get();
		return ($query->num_rows() == 1);
	}

	function save(&$data, $id = false)
	{
		if (!$id && !$this->exists(array('id' => $id, 'city_id' => $data['city_id']))) {
			if ($this->db->insert($this->table_name, $data)) {
				$data['id'] = $this->db->insert_id();
				return true;
			}
		} else {
			$this->db->where('id', $id);
			return $this->db->update($this->table_name, $data);
		}	
		return false;
	}

	function get_all($city_id, $limit=false,$offset=false)
	{
		$this->db->from($this->table_name);
		$this->db->where('city_id', $city_id);
		$this->db->where('is_published',1);
		
		if ($limit) {
			$this->db->limit($limit);
		}
		
		if ($offset) {
			$this->db->offset($offset);
		}
		
		$this->db->order_by('added','desc');
		return $this->db->get();
	}
	/*
	function get_all_by_cat($cat_id)
	{
		$this->db->from($this->table_name);
		$this->db->where('cat_id',$cat_id);
		return $this->db->get();
	}
	*/
	
	function get_all_by_cat($cat_id, $keyword=false, $limit = false, $offset = false)
	{
		$this->db->from($this->table_name);
		$this->db->where('cat_id',$cat_id);
		$this->db->where('is_published',1);
		
		
		if ($keyword && trim($keyword) != "") {
			$this->db->where("(
				name LIKE '%". $keyword ."%' OR 
				description LIKE '%". $keyword ."%' OR 
				search_tag LIKE '%". $keyword ."%' 
			)", NULL, FALSE);
		}
		
		if ($limit) {
			$this->db->limit($limit);
		}
		
		if ($offset) {
			$this->db->offset($offset);
		}
		return $this->db->get();
	}
	
	function get_all_by_keyword_search($city_id = 0, $keyword = false, $limit = false, $offset = false)
	{
		$this->db->from($this->table_name);
		if($city_id != 0) {
			$this->db->where('city_id',$city_id);
		}
		$this->db->where('is_published',1);
		
		
		if ($keyword && trim($keyword) != "") {
			$this->db->where("(
				name LIKE '%". $keyword ."%' OR 
				description LIKE '%". $keyword ."%' OR 
				search_tag LIKE '%". $keyword ."%' 
			)", NULL, FALSE);
		}
		
		if ($limit) {
			$this->db->limit($limit);
		}
		
		if ($offset) {
			$this->db->offset($offset);
		}
		return $this->db->get();
	}
	
	function get_all_by_item_search($city_id = 0, $cat_id = 0, $total_rooms = 0 , $bed_rooms = 0, $bath_rooms = 0, $price_min = 0, $price_max = 0, $meter_min = 0, $meter_max = 0, $for_sell = 0, $for_rent = 0, $limit = false, $offset = false)
	{
		$this->db->from($this->table_name);
		if($city_id != 0) {
			$this->db->where('city_id',$city_id);
		}
		
		if($cat_id != 0) {
			$this->db->where('cat_id',$cat_id);
		}
		
		if($total_rooms != 0) {
			$this->db->where('total_rooms',$total_rooms);
		}
		
		if($bed_rooms != 0) {
			$this->db->where('bed_rooms',$bed_rooms);
		}
		
		if($bath_rooms != 0) {
			$this->db->where('bath_rooms',$bath_rooms);
		}
		
		if($price_min != 0) {
			$this->db->where('price_min >=', $price_min);
		}
		
		if($price_max != 0) {
			$this->db->where('price_max <=', $price_max);
		}
		
		if($meter_min != 0) {
			$this->db->where('sq_meter_min >=',$meter_min);
		}
		
		if($meter_max != 0) {
			$this->db->where('sq_meter_max <=',$meter_max);
		}
		
		if($for_sell == 1 && $for_rent == 1) {
			$this->db->where("(
				for_sell = 1 OR for_rent = 1 
			)", NULL, FALSE);
			
		} else {
			
			if($for_sell != 0) {
				$this->db->where('for_sell',$for_sell);
			}
			
			if($for_rent != 0) {
				$this->db->where('for_rent',$for_rent);
			}
		}
		
		
		
		$this->db->where('is_published',1);
		
		if ($limit) {
			$this->db->limit($limit);
		}
		
		if ($offset) {
			$this->db->offset($offset);
		}
		
		
		//$this->db->get();
		//print_r($this->db->last_query()); die;
		return $this->db->get();
	}

	function get_info($id)
	{
		$query = $this->db->get_where($this->table_name,array('id'=>$id));
		
		if ($query->num_rows()==1) {
			return $query->row();
		} else {
			return $this->get_empty_object($this->table_name);
		}
	}
	
	function get_multiple_info($ids)
	{
		$this->db->from($this->table_name);
		$this->db->where_in($ids);
		return $this->db->get();
	}
	
	function count_all($city_id)
	{
		$this->db->from($this->table_name);
		$this->db->where('city_id', $city_id);
		//$this->db->where('is_published',1);
		return $this->db->count_all_results();
	}
	
	function count_all_by($city_id, $conditions=array())
	{
		$this->db->from($this->table_name);
		$this->db->where('city_id', $city_id);
		
		if ($conditions['sub_cat_id'] != 0) {
			$this->db->where('sub_cat_id', $conditions['sub_cat_id']);
		}
		
		if ($conditions['cat_id'] != 0) {
			$this->db->where('cat_id', $conditions['cat_id']);
		}
		
		if (isset($conditions['searchterm']) && trim($conditions['searchterm']) != "") {
			$this->db->where("(
				 name LIKE '%".$conditions['searchterm']."%' OR 
				 description LIKE '%".$conditions['searchterm']."%' OR 
				 search_tag LIKE '%".$conditions['searchterm']."%' 
			 )", NULL, FALSE);
		}
		
		$this->db->where('is_published',1);
		
		return $this->db->count_all_results();
	}
	
	function get_all_by($city_id, $conditions=array(),$limit=false,$offset=false)
	{
		$this->db->from($this->table_name);
		$this->db->where('city_id', $city_id);
		
		if ($conditions['sub_cat_id'] != 0) {
			$this->db->where('sub_cat_id', $conditions['sub_cat_id']);
		}
		
		if ($conditions['cat_id'] != 0) {
			$this->db->where('cat_id', $conditions['cat_id']);
		}
		
		if (isset($conditions['searchterm']) && trim($conditions['searchterm']) != "") {
			$this->db->where("(
									 name LIKE '%".$conditions['searchterm']."%' OR 
									 description LIKE '%".$conditions['searchterm']."%' OR 
									 search_tag LIKE '%".$conditions['searchterm']."%' 
									 )", NULL, FALSE);
		}
		
		$this->db->where('is_published',1);
		
		if ($limit) {
			$this->db->limit($limit);
		}
		
		if ($offset) {
			$this->db->offset($offset);
		}
		
		$this->db->order_by('added','desc');
		return $this->db->get();
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table_name);
	}
	
	function delete_by_cat($cat_id)
	{
		$this->db->where('cat_id', $cat_id);
		return $this->db->delete($this->table_name);
	}
	
	function delete_by_sub_cat($sub_cat_id)
	{
		$this->db->where('sub_cat_id', $sub_cat_id);
		return $this->db->delete($this->table_name);
	}
	
	function get_popular_items($limit=false, $offset=false)
	{
		$filter = "";
		if ($limit && $offset) {
			$filter = "limit $limit offset $offset";
		} else if ($limit){
			$filter = "limit $limit";
		}
	
		$sql = "
			SELECT count( appuser_id ) as cnt, item_id
			FROM `rt_likes`
			GROUP BY item_id 
			Order By cnt desc
			$filter
		";
	
		$query = $this->db->query($sql);
		return $query;		
	}
	
	function delete_by_city($city_id)
	{
		$this->db->where('city_id', $city_id);
		return $this->db->delete($this->table_name);
	}
	
	function search_by_geo($miles, $user_lat, $user_long, $city_id, $cat_id)
	{
		$query = $this->db->query("SELECT *, ( 3959 * acos( cos( radians('$user_lat') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('$user_long') ) + sin( radians('$user_lat') ) * sin( radians( lat ) ) ) ) AS distance FROM ec_items 
		  Where city_id = '$city_id' and cat_id = '$cat_id'
		  HAVING distance < '$miles' ORDER BY distance LIMIT 0 , 20"); 
		 
		
		return $query;
	}
}
?>