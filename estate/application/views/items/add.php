			<?php
			$this->lang->load('ps', 'english');
			?>
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url(). "/dashboard";?>"><?php echo $this->lang->line('dashboard_label')?></a> <span class="divider"></span></li>
				<li><a href="<?php echo site_url('items');?>"><?php echo $this->lang->line('item_list_label')?></a> <span class="divider"></span></li>
				<li><?php echo $this->lang->line('add_new_item_button')?></li>
			</ul>
			<div class="wrapper wrapper-content animated fadeInRight">
			<?php
			$attributes = array('id' => 'item-form','enctype' => 'multipart/form-data');
			echo form_open(site_url('items/add'), $attributes);
			?>
				<legend><?php echo $this->lang->line('item_info_label')?></legend>
					
				<div class="row">
					<div class="col-sm-6">
							
							<div class="form-group">
								<label><?php echo $this->lang->line('cat_label')?>
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('cat_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<select class="form-control" name="cat_id" id="cat_id">
								<option value=""><?php echo $this->lang->line('select_cat_message')?></option>
								<?php
									$categories = $this->category->get_all($this->city->get_current_city()->id);
									foreach($categories->result() as $cat)
										echo "<option value='".$cat->id."'>".$cat->name."</option>";
								?>
								</select>
							</div>
							
							<div class="form-group">
								<label><?php echo $this->lang->line('agent_name_label')?>
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('agent_name_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<select class="form-control" name="agent_id" id="agent_id">
								<option value=""><?php echo $this->lang->line('select_agent_message')?></option>
								<?php
									$agents = $this->agent->get_all($this->city->get_current_city()->id);
									foreach($agents->result() as $agt)
										echo "<option value='".$agt->id."'>".$agt->name."</option>";
								?>
								</select>
							</div>
							
							<div class="form-group">
								<label><?php echo $this->lang->line('item_name_label')?>
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('item_name_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<?php 
									echo form_input( array(
										'type' => 'text',
										'name' => 'name',
										'id' => 'name',
										'class' => 'form-control',
										'placeholder' => $this->lang->line('item_name_label'),
										'value' => ''
									));
								?>
							</div>
							
							<div class="form-group">
								<label><?php echo $this->lang->line('description_label')?> 
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('item_description_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<textarea class="form-control" name="description" placeholder="<?php echo $this->lang->line('description_label')?>" rows="13"></textarea>
							</div>
							
							<div class="form-group">
								<label><?php echo $this->lang->line('search_tag_label')?> 
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('search_tag_tooltips')?>">
								    	<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<?php 
									echo form_input( array(
										'type' => 'text',
										'name' => 'search_tag',
										'id' => '',
										'class' => 'form-control',
										'placeholder' => $this->lang->line('search_tag_label'),
										'value' => ''
									));
								?>
							</div>
							
														
							<div class="form-group">
								<label><?php echo $this->lang->line('address_label')?> 
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('address_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								
								<?php 
									echo form_input( array(
										'type' => 'text',
										'name' => 'address',
										'id' => '',
										'class' => 'form-control',
										'placeholder' => $this->lang->line('address_label'),
										'value' => ''
									));
								?>
							</div>
							
							<div class="form-group">
									<div class="ibox-content">
							        
							         <label><?php echo $this->lang->line('room_count_label')?> 
							         	<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('room_count_tooltips')?>">
							             	<span class='glyphicon glyphicon-info-sign menu-icon'>
							         	</a>
							         </label>
							         <div class="text-center">
							             
							             <div class="m-r-md inline">
							             <small><?php echo $this->lang->line('total_rooms_label'); ?></small><br/>
							             <input type="text" value="0" name="total_rooms" class="dial m-r-sm" data-fgColor="#1AB394" data-width="85" data-height="85" />
							             </div>
							             
							             <div class="m-r-md inline">
							             <small><?php echo $this->lang->line('bed_rooms_label'); ?></small><br/>
							             <input type="text" value="0" name="bed_rooms" class="dial m-r" data-fgColor="#1AB394" data-width="85" data-height="85"/>
							             </div>
							             
							             <div class="m-r-md inline">
							             <small><?php echo $this->lang->line('bath_rooms_label'); ?></small><br/>
							             <input type="text" value="0"  name="bath_rooms" class="dial m-r" data-fgColor="#1AB394" data-width="85" data-height="85"/>
							             </div>
							         </div>
							    </div>
							</div>
							
						</div>	
						<div class="col-sm-6">
							
							<div class="form-group">
						        <label><?php echo $this->lang->line('find_location_label')?>
						        	<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('find_location_tooltips')?>">
						        		<span class='glyphicon glyphicon-info-sign menu-icon'>
						        	</a>
						        </label><br>
						
						        <?php 
						        	echo form_input( array(
						        		'type' => 'text',
						        		'name' => 'find_location',
						        		'id' => 'find_location',
						        		'class' => 'form-control',
						        		'placeholder' => 'Type & Find Location',
						        		'value' => ''
						        	));
						        ?>
						        
						    </div>
						    
						    <div id="us3" style="width: 550px; height: 300px;"></div>
						    <div class="clearfix">&nbsp;</div>
						    <div class="m-t-small">
						        <div class="form-group">
							        <label><?php echo $this->lang->line('city_lat_label')?>
							        	<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('city_lat_tooltips')?>">
							        		<span class='glyphicon glyphicon-info-sign menu-icon'>
							        	</a>
							        </label>
									<br>
							        <?php 
							        	echo form_input( array(
							        		'type' => 'text',
							        		'name' => 'lat',
							        		'id' => 'lat',
							        		'class' => 'form-control',
							        		'placeholder' => '',
							        		'value' => ''
							        	));
							        ?>
						        </div>
						        
						        <div class="form-group">
							        <label><?php echo $this->lang->line('city_lng_label')?>
							        	<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('city_lng_tooltips')?>">
							        		<span class='glyphicon glyphicon-info-sign menu-icon'>
							        	</a>
							        </label><br>
							        <?php 
							        	echo form_input( array(
							        		'type' => 'text',
							        		'name' => 'lng',
							        		'id' => 'lng',
							        		'class' => 'form-control',
							        		'placeholder' => '',
							        		'value' => ''
							        	));
							        ?>
						        </div>
						    </div>
						    <div class="clearfix"></div>
						
							<div class="form-group">
								<div class="ibox-content"> 	
									<label><?php echo $this->lang->line('price_label') . " (" . $this->lang->line('default_currency_label') ." : ". $this->city->get_current_city()->currency_short_form . ")";?> 
										<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('price_tooltips')?>">
									    	<span class='glyphicon glyphicon-info-sign menu-icon'>
										</a>
									</label>
							    	<?php 
							    		echo form_input( array(
							    			'type' => 'text',
							    			'name' => 'price',
							    			'id' => 'item_price',
							    			'class' => '',
							    			'placeholder' => '',
							    			'value' => ''
							    		));
							    	?>
							    </div>
							</div>
							
							<div class="form-group">
								<div class="ibox-content"> 	
									<label><?php echo $this->lang->line('square_meter_label');?> 
										<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('square_meter_tooltips')?>">
									    	<span class='glyphicon glyphicon-info-sign menu-icon'>
										</a>
									</label>
							    	<?php 
							    		echo form_input( array(
							    			'type' => 'text',
							    			'name' => 'meter',
							    			'id' => 'square_meter',
							    			'class' => '',
							    			'placeholder' => '',
							    			'value' => ''
							    		));
							    	?>
							    </div>
							</div>
							
							
							<div class="form-group">
								<div class="ibox-content"> 	
									<div class="col-sm-3">
										
										<input type="checkbox" id="type" name="type[]" value="s"/>
										
										<label><?php echo $this->lang->line('for_sell_label');?> 
											<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('for_sell_tooltips')?>">
										    	<span class='glyphicon glyphicon-info-sign menu-icon'>
											</a>
										</label> 
								    	
								    </div>
								    
								    <div class="col-sm-3">
								    	
								    	<input type="checkbox" id="type" name="type[]" value="r"/>
								    	
								    	<label><?php echo $this->lang->line('for_rent_label');?> 
								    		<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('for_rent_tooltips')?>">
								    	    	<span class='glyphicon glyphicon-info-sign menu-icon'>
								    		</a>
								    	</label>
								    	
								    </div>
								    <br>
								    <label class="error" for="type[]" generated="true"></label>
							    </div>
							</div>
							
								
						</div>
							
				</div>
				
				<input type="submit" name="save" value="<?php echo $this->lang->line('save_button')?>" class="btn btn-primary"/>
				<input type="submit" name="gallery" value="<?php echo $this->lang->line('save_go_button')?>" class="btn btn-primary"/>
				<a href="<?php echo site_url('items');?>" class="btn btn-primary"><?php echo $this->lang->line('cancel_button')?></a>
			</form>
			
			</div>
			<script>
			$(document).ready(function(){
				$('#item-form').validate({
					rules:{
						name:{
							required: true,
							minlength: 4,
							remote: {
								url: '<?php echo site_url("items/exists");?>',
					        	type: "GET",
					        	data: {
					        		name: function () {
					        			return $('#name').val();
					        		},
					        		cat_id: function() {
					        			return $('#cat_id').val();
					        		}
					        	}
							}
						},
						
						cat_id:{
							required: true
						},
						
						agent_id:{
							required: true
						},
						
						'type[]' :{
							required: true
						}						
						
					},
					messages:{
						name:{
							required: "Please Fill Item Name.",
							minlength: "The length of item Name must be greater than 4",
							remote: "Item Name is already existed in the system"
						},
						cat_id:{
							required: "Please Select Category."
						},
						agent_id:{
							required: "Please Select Agent."
						},
						'type[]':{
							required: "You mush check at least one for Sell or Rent"
						}
						
					}
				});
				
								
				$(function () { $("[data-toggle='tooltip']").tooltip(); });
			});
			
			$(".dial").knob();
			  
			$("#item_price").ionRangeSlider({
	            min: 0,
	            max: 100000000,
	            type: 'double',
	            prefix: '<?php echo $this->city->get_current_city()->currency_symbol;?>',
	            maxPostfix: "+",
	            prettify: false,
	            hasGrid: true
	        });
	        
	        $("#square_meter").ionRangeSlider({
	            min: 0,
	            max: 5000,
	            type: 'double',
	            prefix: '',
	            maxPostfix: "+",
	            prettify: false,
	            hasGrid: true
	        });
	        
	        $('#us3').locationpicker({
	            location: {latitude: 0.0, longitude: 0.0},
	            radius: 300,
	            inputBinding: {
	                latitudeInput: $('#lat'),
	                longitudeInput: $('#lng'),
	                radiusInput: $('#us3-radius'),
	                locationNameInput: $('#find_location')
	            },
	            enableAutocomplete: true,
	            onchanged: function (currentLocation, radius, isMarkerDropped) {
	                // Uncomment line below to show alert on each Location Changed event
	                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
	            }
	        });
			 
			</script>

