			<?php
			$this->lang->load('ps', 'english');
			$attributes = array('class' => 'form-inline','method' => 'POST');
			echo form_open(site_url('reports/analytic'), $attributes);
			?>
			  	<div class="form-group">
			  		<label><?php echo $this->lang->line('cat_label')?></label>
			  		<select class="form-control" name="cat_id" id="cat_id" >
			  		<option><?php echo $this->lang->line('select_cat_message')?></option>
			  		<?php 
			  		foreach($this->category->get_all($this->city->get_current_city()->id)->result() as $category){
			  			echo "<option value='".$category->id."'";
			  			if($cat_id == $category->id) echo " selected ";
			  			echo ">".$category->name."</option>";	
			  		}
			  		?>
			  		</select>
			  	</div>
			  	
			  	<button type="submit" class="btn btn-primary">Generate Report</button>
			<?php echo form_close(); ?>
			<?php if($count > 0):?>
				<div id="chart_div" style="height: 500px;width: 800px;"></div>
				<div id="piechart" style="height: 400px;width: 700px;"></div>
			<?php endif;?>
			
			<script type="text/javascript" src="https://www.google.com/jsapi"></script>
			<script type="text/javascript">
				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback(drawGraphChart);
				google.setOnLoadCallback(drawPieChart);
				
				function drawGraphChart() {
					
					var data = google.visualization.arrayToDataTable(<?php echo $graph_items;?>);
					var options = {
						title: 'Total Touch Counts (All Items From ' + '<?php echo $cat_name;?> )',
						vAxis: {title: 'Items',  titleTextStyle: {color: 'red'}, minValue:0, maxValue:1000},
						colors:['#e57373'],
						backgroundColor: { fill:'transparent' }
					};
					
					var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
					chart.draw(data, options);
				}
				
				function drawPieChart() {
			     	
			     	var data = google.visualization.arrayToDataTable(<?php echo $pie_items;?>);
			     	var options = {
			       		title: 'Top 5 Popular Items From ' + '<?php echo $cat_name;?>)',
			       		backgroundColor: { fill:'transparent' }
			     	};
			
			     	var chart = new google.visualization.PieChart(document.getElementById('piechart'));
			     	chart.draw(data, options);
			   }
			   
			   
			   
			</script>