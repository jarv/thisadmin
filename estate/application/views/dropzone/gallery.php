<?php $this->lang->load('ps', 'english'); ?>

<style>
#actions {
	margin: 2em 0;
}

/* Mimic table appearance */
div.table {
	display: table;
}
div.table .file-row {
	display: table-row;
}
div.table .file-row > div {
	display: table-cell;
	vertical-align: top;
	border-top: 1px solid #ddd;
	padding: 8px;
}
div.table .file-row:nth-child(odd) {
	background: #f9f9f9;
}

/* The total progress gets shown by event listeners */
#total-progress {
	opacity: 0;
	transition: opacity 0.3s linear;
}

/* Hide the progress bar when finished */
#previews .file-row.dz-success .progress {
	opacity: 0;
	transition: opacity 0.3s linear;
}

/* Hide the delete button initially */
#previews .file-row .delete {
	display: none;
}

/* Hide the start and cancel buttons and show the delete button */

#previews .file-row.dz-success .start,
#previews .file-row.dz-success .cancel {
	display: none;
}
#previews .file-row.dz-success .delete {
	display: block;
}

</style>

<ul class="breadcrumb">
	<li>
		<a href="<?php echo site_url(). "/dashboard";?>"><?php echo $this->lang->line('dashboard_label')?></a> 
		<span class="divider"></span>
	</li>
	<li>
		<a href="<?php echo $url;?>">List</a>
		<span class="divider"></span></li>
	<li>
		<a href="<?php echo $url ."/edit/". $id;?>">Edit</a> 
		<span class="divider"></span>
	</li>
	<li>Gallery</li>
</ul>

<div class="wrapper wrapper-content animated fadeInRight">

	<div id="actions" class="row">
		
		<div class="col-md-7">
			<!-- The fileinput-button span is used to style the file input field as button -->
			<span class="btn btn-success fileinput-button">
				<i class="glyphicon glyphicon-plus"></i>
				<span>Add files...</span>
			</span>

			<button type="submit" class="btn btn-primary start">
				<i class="glyphicon glyphicon-upload"></i>
				<span>Start upload</span>
			</button>

			<button type="reset" class="btn btn-warning cancel">
				<i class="glyphicon glyphicon-ban-circle"></i>
				<span>Remove all files</span>
			</button>
		</div>

		<div class="col-md-5">
			<!-- The global file processing state -->
	        <span class="fileupload-process">
	         	<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
	         		<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
	          	</div>
	        </span>
		</div>

	</div>

    <div class="alert alert-success upload-success-message hide">
    </div>

    <p class="well text-center">
    	Drag and Drop the files
    </p>

	<!-- HTML heavily inspired by http://blueimp.github.io/jQuery-File-Upload/ -->
	<div class="table table-striped" class="files" id="previews">

		<div id="template" class="file-row">

			<!-- This is used as the file preview template -->
			<div><span class="preview"><img data-dz-thumbnail /></span></div>
			
			<div>
				<!-- <p class="name" data-dz-name></p> -->
				<textarea class="col-md-12 desc" rows="4"></textarea>
				<strong class="error text-danger" data-dz-errormessage></strong>
			</div>

			<div>
				<p class="size" data-dz-size></p>
				<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
				</div>
			</div>

			<div>
				<button class="btn btn-primary start"><i class="glyphicon glyphicon-upload"></i><span>Start</span></button>

				<button class="btn btn-primary update"><i class="glyphicon glyphicon-upload"></i><span>Update</span></button>

				<button data-dz-remove class="btn cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel</span></button>
			</div>

		</div>

	</div>

</div>

<script>
	// Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
	var previewNode = document.querySelector("#template");
	previewNode.id = "";
	var previewTemplate = previewNode.parentNode.innerHTML;
	previewNode.parentNode.removeChild(previewNode);

	var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
		url: "<?php echo site_url('/dropzone/upload'); ?>", // Set the url
		thumbnailWidth: 80,
		thumbnailHeight: 80,
		parallelUploads: 20,
		previewTemplate: previewTemplate,
		autoQueue: false, // Make sure the files aren't queued until manually added
		previewsContainer: "#previews", // Define the container to display the previews
		clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
	});

	myDropzone.on("addedfile", function(file) {

		// Hookup the start button
		file.previewElement.querySelector(".start").onclick = function() { 
			myDropzone.enqueueFile(file);
			var desc = $(file.previewElement.querySelector(".desc"));

			setTimeout(function(){
				upload( desc, file );
			}, 1500);
		};

		file.previewElement.querySelector(".update").onclick = function() {
			var desc = $(file.previewElement.querySelector(".desc"));
			upload( desc, file );
		}

		// show existing description
		if ( file.desc != undefined ) {
			file.previewElement.querySelector(".desc").innerHTML = file.desc;
		}

		// update buttons' state
		if ( file.id != undefined ) {

			//file.previewElement.querySelector(".start").style.display = 'none';
			var start = $(file.previewElement.querySelector(".start"));
			start.addClass('hide');

			var cancel = $(file.previewElement.querySelector(".cancel"));
			cancel.addClass('btn-danger');
			cancel.find('span').html('Delete');
		} else {
			var update = $(file.previewElement.querySelector(".update"));
			update.addClass('hide');
		}

		// upload function
		var upload = function ( desc, file){
			var value = desc.val();
			console.log(value);
			console.log(file.name);
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('/dropzone/upload_desc'); ?>",
				data: {
					filename: file.name,
					desc: value
				},
				success:function(data){
					console.log(data);
					$('.upload-success-message')
						.html('Successfully updated')
						.removeClass('hide')
						.hide().fadeIn('slow');
				}
			});
		}
	});

	// Update the total progress bar
	myDropzone.on("totaluploadprogress", function(progress) {
		document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
	});

	myDropzone.on("sending", function(file) {
		// Show the total progress bar when upload starts
		document.querySelector("#total-progress").style.opacity = "1";
		// And disable the start button
		file.previewElement.querySelector(".start").remove();

		var update = $(file.previewElement.querySelector(".update"));
		update.removeClass('hide');

		var cancel = $(file.previewElement.querySelector(".cancel"));
		cancel.css('display', 'inline');
		cancel.addClass('btn-danger');
		cancel.find('span').html('Delete');
	});

	// Hide the total progress bar when nothing's uploading anymore
	myDropzone.on("queuecomplete", function(progress) {
		document.querySelector("#total-progress").style.opacity = "0";
	});

	// removing file from upload folder and database
	myDropzone.on("removedfile", function(file){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('/dropzone/upload_remove_photo'); ?>",
			data: {
				name: file.name
			},
			success:function(data){
				console.log('deleted');
				$('.upload-success-message')
					.html('Successfully deleted')
					.removeClass('hide')
					.hide().fadeIn('slow');
			}
		});
	});

	// Setup the buttons for all transfers
	// The "add files" button doesn't need to be setup because the config
	// `clickable` has already been specified.
	document.querySelector("#actions .start").onclick = function() {
		myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
		for ( var i = 0; i < myDropzone.files.length; i++ ) {
			var tmpFile = myDropzone.files[i];
			doTimeout(tmpFile);
		}

		function doTimeout(tmpFile){
			setTimeout(function(){
				var desc = $(tmpFile.previewElement.querySelector(".desc"));
				upload( desc, tmpFile );
			}, 1500);
		}

		// upload function
		var upload = function ( desc, file){
			var value = desc.val();
			console.log(value);
			console.log(file.name);
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('/dropzone/upload_desc'); ?>",
				data: {
					filename: file.name,
					desc: value
				},
				success:function(data){
					console.log(data);
					$('.upload-success-message')
						.html('Successfully updated')
						.removeClass('hide')
						.hide().fadeIn('slow');
				}
			});
		}
	};

	document.querySelector("#actions .cancel").onclick = function() {
		myDropzone.removeAllFiles(true);
	};
</script>

<script>
	var mockFile;
	<?php $imgs = $this->image->get_all_by_type( $_SESSION['parent_id'], $_SESSION['type'] ); ?>
        
   	<?php foreach ($imgs->result() as $img) : ?>
        	
       	mockFile = {
       		id: <?php echo $img->id; ?>,
		    name: "<?php echo $img->path; ?>",
		    desc: "<?php echo $img->description; ?>",
		    size: "2014",
		    status: Dropzone.ADDED,
		    accepted: true
		};

		myDropzone.emit("addedfile", mockFile);                                
		myDropzone.emit("thumbnail", mockFile, "<?php echo base_url( '/uploads/thumbnail/'. $img->path ); ?>");
		myDropzone.emit("complete", mockFile);
		myDropzone.files.push(mockFile);

    <?php endforeach; ?>
    
</script>