			<?php
			$this->lang->load('ps', 'english');
			?>
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url(). "/dashboard";?>"><?php echo $this->lang->line('dashboard_label')?></a> <span class="divider"></span></li>
				<li><a href="<?php echo site_url('agents');?>"><?php echo $this->lang->line('agent_list_label')?></a> <span class="divider"></span></li>
				<li><?php echo $this->lang->line('add_new_agent_button')?></li>
			</ul>
			<div class="wrapper wrapper-content animated fadeInRight">
			<?php
			$attributes = array('id' => 'agent-form','enctype' => 'multipart/form-data');
			echo form_open(site_url('agents/add'), $attributes);
			
			?>
				<legend><?php echo $this->lang->line('agent_info_lable')?></legend>
				
				<div class="row">
					<div class="col-sm-8">
							<div class="form-group">
								<label><?php echo $this->lang->line('agent_name_label')?>
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('agent_name_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<?php 
									echo form_input( array(
										'type' => 'text',
										'name' => 'name',
										'id' => 'name',
										'class' => 'form-control',
										'placeholder' => 'Agent Name',
										'value' => ''
									));
								?>
							</div>
							
							<div class="form-group">
								<label><?php echo $this->lang->line('description_label')?>
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('agent_description_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<textarea class="form-control" name="description" placeholder="Description" rows="9"></textarea>
							</div>
							
							<div class="form-group">
								<label><?php echo $this->lang->line('phone_label')?>
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('phone_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<?php 
									echo form_input( array(
										'type' => 'text',
										'name' => 'phone',
										'id' => 'phone',
										'class' => 'form-control',
										'placeholder' => 'Phone Number',
										'value' => ''
									));
								?>
							</div>
							
							<div class="form-group">
								<label><?php echo $this->lang->line('email_label')?>
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('email_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<?php 
									echo form_input( array(
										'type' => 'text',
										'name' => 'email',
										'id' => 'email',
										'class' => 'form-control',
										'placeholder' => 'Email',
										'value' => ''
									));
								?>
							</div>
							
							<div class="form-group">
								<label><?php echo $this->lang->line('agent_photo_label')?>
									<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('agent_photo_tooltips')?>">
										<span class='glyphicon glyphicon-info-sign menu-icon'>
									</a>
								</label>
								<input class="btn" type="file" name="images1">
							</div>	
							
					</div>
				</div>
				
				<hr/>
				
				<input type="submit" name="save" value="<?php echo $this->lang->line('save_button')?>" class="btn btn-primary"/>
				<a href="<?php echo site_url('agents');?>" class="btn btn-primary"><?php echo $this->lang->line('cancel_button')?></a>
			</form>
			</div>
			<script>
			$(document).ready(function(){
				$('#agent-form').validate({
					rules:{
						name:{
							required: true,
							minlength: 3
						}
					},
					messages:{
						name:{
							required: "Please Fill Name.",
							minlength: "The length of name must be greater than 3"
						}
					}
				});
			});
			
			$(function () { $("[data-toggle='tooltip']").tooltip(); });
			
			</script>

