			<?php
			$this->lang->load('ps', 'english');
			?>
			<ul class="breadcrumb">
				<li><a href="<?php echo site_url(). "/dashboard";?>"><?php echo $this->lang->line('dashboard_label')?></a> <span class="divider"></span></li>
				<li><a href="<?php echo site_url('agents');?>"><?php echo $this->lang->line('agent_list_label')?></a> <span class="divider"></span></li>
				<li><?php echo $this->lang->line('update_agent_label')?></li>
			</ul>
			<div class="wrapper wrapper-content animated fadeInRight">
			<?php
			$this->lang->load('ps', 'english');
			$attributes = array('id' => 'agent-form','enctype' => 'multipart/form-data');
			echo form_open(site_url("agents/edit/".$agent->id), $attributes);
			?>
				<legend><?php echo $this->lang->line('agent_info_lable')?></legend>
				
				<div class="row">
					<div class="col-sm-8">
						<div class="form-group">
							<label><?php echo $this->lang->line('agent_name_label')?>
								<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('agent_name_tooltips')?>">
									<span class='glyphicon glyphicon-info-sign menu-icon'>
								</a>
							</label>
							<?php 
								echo form_input( array(
									'type' => 'text',
									'name' => 'name',
									'id' => 'name',
									'class' => 'form-control',
									'placeholder' => 'Agent Name',
									'value' => $agent->name
								));
							?>
						</div>
						
						<div class="form-group">
							<label><?php echo $this->lang->line('description_label')?>
								<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('feed_description_tooltips')?>">
									<span class='glyphicon glyphicon-info-sign menu-icon'>
								</a>
							</label>
							<textarea class="form-control" name="description" placeholder="Description" rows="9"><?php echo $agent->description;?></textarea>
						</div>
						
						<div class="form-group">
							<label><?php echo $this->lang->line('publish_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('publish_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
							: 
							</label>
							<?php
								echo form_checkbox("is_published",$agent->is_published,$agent->is_published);
							 ?>
						</div>
						
						<div class="form-group">
							<label><?php echo $this->lang->line('phone_label')?>
								<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('phone_tooltips')?>">
									<span class='glyphicon glyphicon-info-sign menu-icon'>
								</a>
							</label>
							<?php 
								echo form_input( array(
									'type' => 'text',
									'name' => 'phone',
									'id' => 'phone',
									'class' => 'form-control',
									'placeholder' => 'Phone Number',
									'value' => $agent->phone
								));
							?>
						</div>
						
						<div class="form-group">
							<label><?php echo $this->lang->line('email_label')?>
								<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('email_tooltips')?>">
									<span class='glyphicon glyphicon-info-sign menu-icon'>
								</a>
							</label>
							<?php 
								echo form_input( array(
									'type' => 'text',
									'name' => 'email',
									'id' => 'email',
									'class' => 'form-control',
									'placeholder' => 'Email',
									'value' => $agent->email
								));
							?>
						</div>
						
						<label><?php echo $this->lang->line('agent_photo_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo $this->lang->line('agent_photo_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label> <a class="btn btn-primary btn-upload pull-right" data-toggle="modal" data-target="#uploadImage"><?php echo $this->lang->line('replace_photo_button')?></a>
					
						<hr/>					
						<?php
							$images = $this->image->get_all_by_type($agent->id, 'agent')->result();
							if(count($images) > 0):
						?>
							<div class="row">
							<?php
								$i= 0;
								foreach ($images as $img) {
									if ($i>0 && $i%3==0) {
										echo "</div><div class='row'>";
									}
									
									echo '<div class="col-md-4" style="height:100"><div class="thumbnail">'.
										'<img src="'.base_url('uploads/thumbnail/'.$img->path).'"><br/>'.
										'<p class="text-center">'.
										'<a data-toggle="modal" data-target="#deletePhoto" class="delete-img" id="'.$img->id.'"   
											image="'.$img->path.'">Remove</a></p>'.
										'</div></div>';
								   $i++;
								}
							?>
							</div>
						
						<?php
							endif;
						?>
						
						<input type="submit" value="<?php echo $this->lang->line('update_button')?>" class="btn btn-primary"/>
						
						<a href="<?php echo site_url('agents');?>" class="btn btn-primary"><?php echo $this->lang->line('cancel_button')?></a>
					</div>
				</div>
			</form>
			</div>	
			
			<div class="modal fade"  id="uploadImage">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
							</button>
							<h4 class="modal-title"><?php echo $this->lang->line('replace_photo_button')?></h4>
						</div>
						<?php
						$attributes = array('id' => 'upload-form','enctype' => 'multipart/form-data');
						echo form_open(site_url("agents/upload/".$agent->id), $attributes);
						?>
							<div class="modal-body">
								<div class="form-group">
									<label><?php echo $this->lang->line('upload_photo_label')?></label>
									<input type="file" name="images1">
								</div>
							</div>
							<div class="modal-footer">
								<input type="submit" value="Upload" class="btn btn-primary"/>
								<a type="button" class="btn btn-primary" data-dismiss="modal"><?php echo $this->lang->line('cancel_button')?></a>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<div class="modal fade"  id="deletePhoto">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title"><?php echo $this->lang->line('delete_cover_photo_label')?></h4>
						</div>
						<div class="modal-body">
							<p><?php echo $this->lang->line('delete_photo_confirm_message')?></p>
						</div>
						<div class="modal-footer">
							<a type="button" class="btn btn-primary btn-delete-image"><?php echo $this->lang->line('yes_button')?></a>
							<a type="button" class="btn btn-primary" data-dismiss="modal"><?php echo $this->lang->line('cancel_button')?></a>
						</div>
					</div>
				</div>			
			</div>
					
			<script>
				$(document).ready(function(){
					$('#agent-form').validate({
						rules:{
							title:{
								required: true,
								minlength: 3
							}
						},
						messages:{
							name:{
								required: "Please fill agent name.",
								minlength: "The length of feed name must be greater than 3"
							}
						}
					});				
				});
				
				$('.delete-img').click(function(e){
					e.preventDefault();
					var id = $(this).attr('id');
					var image = $(this).attr('image');
					var action = '<?php echo site_url('agents/delete_image/'.$agent->id);?>/' + id + '/' + image;
					$('.btn-delete-image').attr('href', action);
				});
				
				$(function () { $("[data-toggle='tooltip']").tooltip(); });
				
				
			</script>

