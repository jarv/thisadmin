<?php
require_once('main.php');

class Agents extends Main
{
	function __construct()
	{
		parent::__construct('agents');
        $this->load->model('image');
        $this->load->library('uploader');
	}
	
	function index()
	{
		$this->session->unset_userdata('searchterm');
	
		$pag = $this->config->item('pagination');
		$pag['base_url'] = site_url('agents/index');
		
		$pag['total_rows'] = $this->agent->count_all($this->get_current_city()->id);
		$data['agents'] = $this->agent->get_all($this->get_current_city()->id, $pag['per_page'], $this->uri->segment(3));
		$data['pag'] = $pag;
		
		$content['content'] = $this->load->view('agents/list',$data,true);		
		
		$this->load_template($content);
	}
	
	function add()
	{
		if(!$this->session->userdata('is_city_admin')) {
		      $this->check_access('add');
		}
		
		if ($this->input->server('REQUEST_METHOD')=='POST') {
			
			$upload_data = $this->uploader->upload($_FILES);
			
			if (!isset($upload_data['error'])) {
				$agent_data = array(
					'city_id'     => $this->get_current_city()->id,
					'name'        => htmlentities($this->input->post('name')),
					'description' => htmlentities($this->input->post('description')),
					'phone'       => htmlentities($this->input->post('phone')),
					'email'       => htmlentities($this->input->post('email')),
					'is_published' => 1);
					
				if ($this->agent->save($agent_data)) {
					foreach ($upload_data as $upload) {
						$image = array(
							'parent_id'=>$agent_data['id'],
							'type' => 'agent',
							'description' => "",
							'path' => $upload['file_name'],
							'width'=>$upload['image_width'],
							'height'=>$upload['image_height']
						);
						$this->image->save($image);
						$this->session->set_flashdata('success','Agent is successfully added.');
					}
				} else {
					$this->session->set_flashdata('error','Database error occured.Please contact your system administrator.');
				}
				
				redirect(site_url('agents'));	
					
			} else {
				$data['error'] = $upload_data['error'];
			}
		}
		
		$content['content'] = $this->load->view('agents/add',array(),true);
		
		$this->load_template($content);
		
		
	}

	function gallery( $id ) 
	{
		session_start();
		$_SESSION['parent_id'] = $id;
		$_SESSION['type'] = 'agent';

		$data = array(
			'id' => $id,
			'url' => site_url('agents')
		);
		
		$upload_lib_name = $this->config->item( 'upload_library_name' );

		if ( $upload_lib_name == DROPZONE ) {
			$content['content'] = $this->load->view('dropzone/gallery', $data, true);
		} else {
			$content['content'] = $this->load->view('jupload/gallery', $data, true);
		}
		
		$this->load_template($content);
	}
	
	function edit($agent_id=0)
	{
		if(!$this->session->userdata('is_city_admin')) {
		    $this->check_access('edit');
		}
		
		if ($this->input->server('REQUEST_METHOD')=='POST') {

			$data = array();
			foreach ( $this->input->post() as $key => $value ) {
				$data[$key] = htmlentities($value);
			}

			if ($this->agent->save($data,$agent_id)) {
				$this->session->set_flashdata('success','Agent is successfully updated.');
			} else {
				$this->session->set_flashdata('error','Database error occured.Please contact your system administrator.');
			}
			redirect(site_url('agents'));
		}
		
		$data['agent'] = $this->agent->get_info($agent_id);
		
		$content['content'] = $this->load->view('agents/edit',$data,true);		
		
		$this->load_template($content);
	}	
	
	function search()
	{
		$search_term = $this->searchterm_handler(array(
			"searchterm"=>htmlentities($this->input->post('searchterm'))
		));
		$data = $search_term;
		
		$pag = $this->config->item('pagination');
		
		$pag['base_url'] = site_url('agents/search');
		$pag['total_rows'] = $this->agent->count_all_by($this->get_current_city()->id, $search_term);
		
		$data['agents'] = $this->agent->get_all_by($this->get_current_city()->id, $search_term,$pag['per_page'],$this->uri->segment(3));
		$data['pag'] = $pag;
		
		$content['content'] = $this->load->view('agents/search',$data,true);		
		
		$this->load_template($content);
	}
	
	function searchterm_handler($searchterms = array())
	{
		$data = array();
		
		if ($this->input->server('REQUEST_METHOD')=='POST') {
			foreach ($searchterms as $name=>$term) {
				if ($term && trim($term) != " ") {
					$this->session->set_userdata($name,$term);
					$data[$name] = $term;
				} else {
					$this->session->unset_userdata($term);
					$data[$name] = "";
				}
			}
		} else {
			foreach ($searchterms as $name=>$term) {
				if ($this->session->userdata($name)) {
					$data[$name] = $this->session->userdata($name);
				} else { 
					$data[$name] = "";
				}
			}
		}
		return $data;
	}
	
	function delete($agent_id=0)
	{
		if(!$this->session->userdata('is_city_admin')) {
		     $this->check_access('delete');
		}
		
		$images = $this->image->get_all_by_type($agent_id, 'agent');
		foreach ($images->result() as $image) {
			$this->image->delete($image->id);
			@unlink('./uploads/'.$image->path);
			@unlink('./uploads/thumbnail/'.$image->path);
		}
		
		if ($this->agent->delete($agent_id)) {
			$this->session->set_flashdata('success','Agent is successfully deleted.');
		} else {
			$this->session->set_flashdata('error','Database error occured.Please contact your system administrator.');
		}
		redirect(site_url('agents'));
	}
	
	function publish($id = 0)
	{
		if(!$this->session->userdata('is_city_admin')) {
			$this->check_access('publish');
		}
		
		$agent_data = array(
			'is_published'=> 1
		);
			
		if ($this->agent->save($agent_data,$id)) {
			echo 'true';
		} else {
			echo 'false';
		}
	}
	
	function unpublish($id = 0)
	{
		if(!$this->session->userdata('is_city_admin')) {
			$this->check_access('publish');
		}
		
		$agent_data = array(
			'is_published'=> 0
		);
			
		if ($this->agent->save($agent_data,$id)) {
			echo 'true';
		} else {
			echo 'false';
		}
	}
	
	function upload($agent_id=0)
	{
		if(!$this->session->userdata('is_city_admin')) {
		    $this->check_access('edit');
		}
		
		$upload_data = $this->uploader->upload($_FILES);
		
		if (!isset($upload_data['error'])) {
	
			@unlink('./uploads/'.$this->image->get_info_parent_type($agent_id,'agent')->path);
			@unlink('./uploads/thumbnail/'.$this->image->get_info_parent_type($agent_id,'agent')->path);
			$this->image->delete_by_parent($agent_id,'agent');
			
			foreach ($upload_data as $upload) {
				$image = array(
					'parent_id'=> $agent_id,
					'type' => 'agent',
					'description' => "",
					'path' => $upload['file_name'],
					'width'=>$upload['image_width'],
					'height'=>$upload['image_height']
				);
				$this->image->save($image);
				redirect(site_url('agents/edit/' . $agent_id));
			}
			
		} else {
			$data['error'] = $upload_data['error'];
		}
		
		$data['agent'] = $this->agent->get_info($agent_id);
		
		$content['content'] = $this->load->view('agents/edit',$data,true);		
		$this->load_template($content);
	}
	
	function delete_image($agent_id, $image_id, $image_name)
	{
		if(!$this->session->userdata('is_city_admin')) {
		    $this->check_access('edit');
		}
		
		if ($this->image->delete($image_id)) {
			unlink('./uploads/'.$image_name);
			unlink('./uploads/thumbnail/'.$image_name);
			$this->session->set_flashdata('success','Agent photo is successfully deleted.');
		} else {
			$this->session->set_flashdata('error','Database error occured.Please contact your system administrator.');
		}
		redirect(site_url('agents/edit/' . $agent_id));
	}
}