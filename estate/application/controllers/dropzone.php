<?php

class Dropzone extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function upload()
	{
		session_start();

		# load library
		$this->load->library('upload');
		$this->load->library('image_lib');

		# initialize libraries
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = '*';
		$config['overwrite'] = FALSE;

		$this->upload->initialize($config);
    	$this->image_lib->clear();

		# if there is no error in file
		if ( empty( $_FILES )) {
			echo 'file error';
			return;
		}

		# upload the file
		if ( ! $this->upload->do_upload( 'file' )) {
			$error = $this->upload->display_errors();
			return;
		}
    	$image_data = $this->upload->data();

		# create thumbnail
		$config = array(
			'source_image' => $image_data['full_path'],
			'new_image' => './uploads/thumbnail',
			'maintain_ration' => true,
			'width' => 80,
			'height' => 60
		);
		$this->image_lib->initialize($config);
		$this->image_lib->resize();

        # save to database
        $image = array(
			'parent_id'=> $_SESSION['parent_id'],
			'type' => $_SESSION['type'],
			'path' => $image_data['file_name'],
			'width' => $image_data['image_width'],
			'height' => $image_data['image_height']
		);
        $this->image->save( $image );
	}

	function upload_desc()
	{
		$filename = $_REQUEST[ 'filename' ];
		$desc = $_REQUEST[ 'desc' ];
		
		$this->image->save_desc( $desc, $filename );
	}

	function upload_remove_photo()
	{
		$filename = $_REQUEST['name'];

		if ( file_exists( './uploads/'. $filename )) {
    		unlink( './uploads/'. $filename );
    	}

    	if ( file_exists( './uploads/thumbnail/'. $filename )) {
    		unlink( './uploads/thumbnail/'. $filename );
    	}

    	$this->image->delete_by_name( $filename );
	}
}