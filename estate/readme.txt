EstateCore is Full Application solution with Backend Management System.

Used Technology For Android:
- Native Android Development 
- Java JDK 
- Android SDK 
- Android Studio
- GSON 
- Google Map 
- Google Cloud Messaging 
- CodeIgniter 
- Bootstrap 
- RESTFUL API
- Google Chart API
- JSON 

Used Technology For iOS:
- Swift
- Alamofire 
- CoreLocation 
- xCode7.1 and above
- Apple Map 
- Push Notification 
- CodeIgniter 
- Bootstrap 
- RESTFUL API
- Google Chart API
- JSON 

Release Note
- v(1.0.0)
- 10 May 2016