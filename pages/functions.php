<?php

function connectToDB() {
	global $link, $dbhost, $dbuser, $dbpass, $dbname;
	($link = mysqli_connect("$dbhost", "$dbuser", "$dbpass")) || die("Couldn't connect to MySQL");

	mysqli_select_db($link, "$dbname") || die("Couldn't open db: $dbname. Error if any was: ".mysqli_error() );
} 

function displayErrors($messages) {
	print("<b>There were problems with the previous action.  Following is a list of the error messages generated:</b>\n<ul>\n");

	foreach($messages as $msg){
		print("<li>$msg</li>\n");
	}
	print("</ul>\n");
} 
function checkLoggedIn($status){
	switch($status){
		case "yes":
			if(!isset($_SESSION["loggedIn"])){
				header("Location: main.php");
				exit;
			}
			break;

		case "no":
			if(isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] === true ){
				header("Location: index.php");
			}
			break;
	}
	return true;
} 
function checkLoggedIn1($status){
	switch($status){
		// if yes, check user is logged in:
		// ie for actions where, yes, user must be logged in(!)
		case "yes":
			if(!isset($_SESSION["loggedIn"])){
				header("Location: index.php");
				exit;
			}
			break;

		// if no, check NOT logged in:
		// ie for actions where user can't already be logged in
		// (ie for joining up or logging in)
		case "no":
			/*
				The '===' operator differs slightly from the '=='
				equality operator.

				$a === $b if and only if $a is equal to $b AND
				$a is the same variable type as $b.

				for example, if:

				$a="2";	<-- $a is a string here
				$b=2;	<-- $b is an integer here

				then this test returns false:
				if($a===$b)

				whereas this test returns true:
				if($a==$b)
			*/
			if(isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] === true ){
				header("Location: main.php");
			}
			break;
	}
	// if got here, all ok, return true:
	return true;
} // end func checkLoggedIn($status)




/******************************************************\
 * Function Name : doCSS()
 *
 * Task : output the CSS for the screens
 *
 * Arguments :
 *
 * Returns :
 *
 \******************************************************/


# function validates HTML form field data passed to it:
function field_validator($field_descr, $field_data,
  $field_type, $min_length="", $max_length="",
  $field_required=1) {
	/*
	Field validator:
	This is a handy function for validating the data passed to
	us from a user's <form> fields.

	Using this function we can check a certain type of data was
	passed to us (email, digit, number, etc) and that the data
	was of a certain length.
	*/
	
	# array for storing error messages
	global $messages;

	# first, if no data and field is not required, just return now:
	if(!$field_data && !$field_required){ return; }

	# initialize a flag variable - used to flag whether data is valid or not
	$field_ok=false;

	# this is the regexp for email validation:
	$email_regexp="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|";
	$email_regexp.="(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

	# a hash array of "types of data" pointing to "regexps" used to validate the data:
	$data_types=array(
		"email"=>$email_regexp,
		"digit"=>"^[0-9]$",
		"number"=>"^[0-9]+$",
		"alpha"=>"^[a-zA-Z]+$",
		"alpha_space"=>"^[a-zA-Z ]+$",
		"alphanumeric"=>"^[a-zA-Z0-9]+$",
		"alphanumeric_space"=>"^[a-zA-Z0-9 ]+$",
		"string"=>""
	);

	# check for required fields
	if ($field_required && empty($field_data)) {
		$messages[] = "$field_descr is a required field.";
		return;
	}

	# if field type is a string, no need to check regexp:
	if ($field_type == "string") {
		$field_ok = true;
	} else {
		# Check the field data against the regexp pattern:
		$field_ok = ereg($data_types[$field_type], $field_data);
	}

	# if field data is bad, add message:
	if (!$field_ok) {
		$messages[] = "Please enter a valid $field_descr.";
		return;
	}

	# field data min length checking:
	if ($field_ok && ($min_length > 0)) {
		if (strlen($field_data) < $min_length) {
			$messages[] = "$field_descr is invalid, it should be at least $min_length character(s).";
			return;
		}
	}

	# field data max length checking:
	if ($field_ok && ($max_length > 0)) {
		if (strlen($field_data) > $max_length) {
			$messages[] = "$field_descr is invalid, it should be less than $max_length characters.";
			return;
		}
	}
}
function checkbox_value($name) {
    return (isset($_POST[$name]) ? 'yes' : 'no');
}

?>
