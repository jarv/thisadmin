<?php
include_once("config.php");
if (!isset($_SESSION['rsUser'])) {
$msg = "Username and/or Password incorrect!";
header('Location: index.php?msg='.$msg.'');
}

	$tableName="pubs";		
	$targetpage = "pubs.php"; 	
	$limit = 25; 
	
	if($myTown != 'ALL'){
		$query = "SELECT COUNT(*) as num FROM $tableName WHERE rsTown = '$myTown'";
	} else {
		$query = "SELECT COUNT(*) as num FROM $tableName";
	}
	$total_pages = mysqli_fetch_array(mysqli_query($link,$query));
	$total_pages = $total_pages['num'];

	$stages = 3;
	$page = mysqli_real_escape_string($link,$_REQUEST['pagec']);
	if( isset($_REQUEST['pagec']) && ctype_digit($_REQUEST['pagec']) ) {
      $page = (int) $_GET['pagec'];
      $start = ($page - 1) * $limit;
}else{
      $start = 0;   
}
	
    // Get page data
    if($myTown != 'ALL'){
		$query1 = "SELECT * FROM $tableName WHERE rsTown = '$myTown'";
		//$query2 = "SELECT * FROM $tableName ORDER BY rsPubName Asc";
		$query3 = "SELECT DISTINCT rsTown FROM $tableName WHERE rsTown = '$myTown' ORDER BY rsTown ASC";
		$query4 = "SELECT DISTINCT rsCounty FROM $tableName WHERE rsTown = '$myTown' ORDER BY rsCounty ASC";
		//$query5 = "SELECT DISTINCT rsPubName FROM $tableName";
		$query6 = "SELECT DISTINCT rsTown FROM $tableName WHERE rsTown = '$myTown' ORDER BY rsTown ASC";
		$query7 = "SELECT DISTINCT rsCounty FROM $tableName WHERE rsTown = '$myTown' ORDER BY rsCounty ASC";
	} else {
		$query1 = "SELECT * FROM $tableName";
		//$query2 = "SELECT * FROM $tableName ORDER BY rsPubName Asc";
		$query3 = "SELECT DISTINCT rsTown FROM $tableName ORDER BY rsTown ASC";
		$query4 = "SELECT DISTINCT rsCounty FROM $tableName ORDER BY rsCounty ASC";
		//$query5 = "SELECT DISTINCT rsPubName FROM $tableName";
		$query6 = "SELECT DISTINCT rsTown FROM $tableName ORDER BY rsTown ASC";
		$query7 = "SELECT DISTINCT rsCounty FROM $tableName ORDER BY rsCounty ASC";
	}
	if (!isset($_REQUEST['sortby']))
	{
	$_REQUEST['sortby'] = "ORDER BY PUBID";
	}
	if (!isset($_REQUEST['sorting']))
	{
	$_REQUEST['sorting'] = "ASC";
	}

	//if($_REQUEST['sortby'] != ""){
	//	$_SESSION['sortby'] = $REQUEST['sortby'];
	//}
	//if($_REQUEST['sorting'] != ""){
	//	$_SESSION['sorting'] = $REQUEST['sorting'];
	//}
	
	switch ($_REQUEST['sortby']) {
	    case "rsCounty";
	        $sqlsort = " ORDER BY rsCounty";
	        break;
	    case "OrderDate";
	        $sqlsort = " ORDER BY OrderDate";
	        break;
	    case "PUBID";
	        $sqlsort = " ORDER BY PUBID";
	        break;
	    default:
	    	$sqlsort = " ORDER BY PUBID";
	    	break;
	}
	
	switch ($_REQUEST['sorting']) {
	    case "ASC";
	        $sqlorder = " ASC";
	        break;
	    case "DESC";
	        $sqlorder = " DESC";
	        break;
	    default:
	    	$sqlorder = " ASC";
	    	break;
	}
	$query1 = $query1.$sqlsort.$sqlorder." LIMIT $start, $limit";
//echo $query1;
	$result = mysqli_query($link,$query1);
	//$result2 = mysqli_query($query2);
	$result3 = mysqli_query($link,$query3);
	$result4 = mysqli_query($link,$query4);
	//$result5 = mysqli_query($query5);
	$result6 = mysqli_query($link,$query6);
	$result7 = mysqli_query($link,$query7);
		$pubsquery = "SELECT * FROM pubs WHERE pubs.rsTown = '$myTown'";
    $pubs = mysqli_query($link,$pubsquery);


	// Initial page num setup
	if ($page == 0){$page = 1;}
	$prev = $page - 1;	
	$next = $page + 1;							
	$lastpage = ceil(mysqli_num_rows($pubs)/$limit);		
	$LastPagem1 = $lastpage - 1;					
	
	
	$paginatecust = '';
	if($lastpage > 1)
	{	
	

		$paginatecust .= "<div class='pagination dataTables_paginate paging_simple_numbers' id='dataTables-example_paginate'>";
		// Previous
		if ($page > 1){
			$paginatecust.= "<ul class='pagination'>";
			$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$prev&pageu=1' title='Previous Page'>&laquo; Previous</a></li>";
		}else{
			$paginatecust.= "<li class='paginate_button'><span class='disabled' title='Previous page'>&laquo; Previous</span></li>";	}
			

		
		// Pages	
		if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page){
					$paginatecust.= "<li class='paginate_button active'><a href='#' class='number current'>$counter</a></li>";
				}else{
					$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$counter&pageu=1' class='number'>$counter</a></li>";}					
			}
		}
		elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
		{
			// Beginning only hide later pages
			if($page < 1 + ($stages * 2))		
			{
				for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
				{
					if ($counter == $page){
						$paginatecust.= "<li class='paginate_button active'><a href='#' class='number current'>$counter</a></li>";
					}else{
						$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$counter&pageu=1' class='number'>$counter</a></li>";}					
				}
				$paginatecust.= "<li class='paginate_button'>...</li>";
				$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$LastPagem1&pageu=1' class='number'>$LastPagem1</a></li>";
				$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$lastpage&pageu=1' class='number'>$lastpage</a></li>";		
			}
			// Middle hide some front and some back
			elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
			{
				$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=1&pageu=1' class='number'>1</a></li>";
				$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=2&pageu=1' class='number'>2</a></li>";
				$paginatecust.= "<li class='paginate_button'>...</li>";
				for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
				{
					if ($counter == $page){
						$paginatecust.= "<li class='paginate_button active'><span class='number current'>$counter</span></li>";
					}else{
						$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$counter&pageu=1' class='number'>$counter</a></li>";}					
				}
				$paginatecust.= "<li class='paginate_button'>...</li>";
				$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$LastPagem1&pageu=1' class='number'>$LastPagem1</a></li>";
				$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$lastpage&pageu=1' class='number'>$lastpage</a></li>";		
			}
			// End only hide early pages
			else
			{
				$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=1&pageu=1' class='number'>1</a></li>";
				$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=2&pageu=1' class='number'>2</a></li>";
				$paginatecust.= "<li class='paginate_button'>...</li>";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page){
						$paginatecust.= "<li class='paginate_button active'><a href='#' class='number current'>$counter</a></li>";
					}else{
						$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$counter&pageu=1' class='number'>$counter</a></li>";}					
				}
			}
		}
					
				// Next
		if ($page < $counter - 1){ 
			$paginatecust.= "<li class='paginate_button'><a href='$targetpage?pagec=$next&pageu=1' title='Next Page'>Next Page &raquo;</a></li>";
		}else{
			$paginatecust.= "<li class='paginate_button'><span class='disabled' title='Next Page'>Next Page &raquo;</span></li>";
			}
		$paginatecust.= "</ul>";	
		$paginatecust.= "</div>";
	
} 
 
?>