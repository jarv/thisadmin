<?php
session_start();
include_once("config.php");
checkLoggedIn("no");
if (!isset($_REQUEST['Remember']))
{
$Remember = 0;
} else {
$Remember = $_REQUEST['Remember'];
}
if (!isset($_REQUEST['msg']))
{
$_REQUEST['msg'] = "nothing";
} 
if (isset($_SESSION['rsUser'])) {
header('Location: main.php');
}
if(isset($_POST["submit"])) {

	// username and password sent from form
	$myusername=$_POST['rsUser'];
	$mypassword=$_POST['rsPass'];
	$rsTown = $_POST['rsTown'];
	// To protect MySQL injection
	$myusername = stripslashes($myusername);
	$mypassword = stripslashes($mypassword);
	$rsTown = stripslashes($rsTown);
	$myusername = mysqli_real_escape_string($link,$myusername);
	$mypassword = mysqli_real_escape_string($link,$mypassword);
	$rsTown = mysqli_real_escape_string($link,$rsTown);
	$sql="SELECT * FROM theseusers WHERE rsUser='$myusername' and rsPass='$mypassword' and rsTown = '$rsTown'";
	$result=mysqli_query($link,$sql);
	
	// Mysql_num_row is counting table row
	$count=mysqli_num_rows($result);
	// If result matched $myusername and $mypassword, table row must be 1 row
	//echo $count;
	if($count==1){
	// Register $myusername, $mypassword and redirect
	$_SESSION['rsUser'] = $myusername;
	$_SESSION['rsPass'] = $mypassword;
	$_SESSION['rsTown'] = $rsTown;
	
	if($Remember='1'){
                setcookie($_SESSION['rsUser'], time()+60*60*24*7);
                //RememberME check?, save the ID in a cookie.
            } 
	header("location:main.php");
	}
	else {
	$msg = "Username and/or Password incorrect!";
// 	  header("Location: index.php?msg=$msg");
	  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ThisAdmin</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form method="post" name="login" action="<?php print $_SERVER["PHP_SELF"]; ?>">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="User" name="rsUser" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="rsPass" type="password">
                                </div>
                                <?php 
                					$townsquery = "SELECT DISTINCT rsTown FROM theseusers";
					                $towns = mysqli_query($link,$townsquery);
				                ?>
                                <p>
									<label>Town</label>
									<select class="form-control" class="text-select" name="rsTown">
									<?php while($townrow = mysqli_fetch_array($towns)) {?>
										<option value="<?php echo $townrow['rsTown'];?>"><?php echo $townrow['rsTown'];?></option>
									<?php }?>
									</select>
								</p>

								<div class="clear"></div>

                                <div class="checkbox">
                                    <label>
                                        <input name="Remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input class="btn btn-lg btn-primary" type="submit" name="submit" value="Sign In" />
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
