<?php
session_start();
include_once("config.php");
$myTown = $_SESSION['rsTown'];
if (!isset($_SESSION['rsUser'])) {
$msg = "Username and/or Password incorrect!";
header('Location: index.php?msg='.$msg.'');
}

if (!isset($_REQUEST['msg']))
{
$_REQUEST['msg'] = "nothing";
} 
if (!isset($_REQUEST['pagec']))
{
$_REQUEST['pagec'] = 1;
} 
if (!isset($_REQUEST['pageu']))
{
$_REQUEST['pageu'] = 1;
} 	
include_once("pubs-pagination.php");
//include_once("events-pagination.php");	
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ThisAdmin</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">ThisAdmin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <?php 
                $eventsquery = "SELECT * FROM pubs INNER JOIN pubs_events ON pubs_events.eventvenue=pubs.PUBID WHERE pubs.rsTown = '$myTown' ORDER BY eventdate ASC LIMIT 0,10";
                $events = mysqli_query($link,$eventsquery);
                ?>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-calendar fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                    <?php while($eventrow = mysql_fetch_array($events)) {?>
                        <li>
                            <a href="viewevent.php?eventid=<?php echo $eventrow['eventid'];?>">
                                <div>
                                    <i class="fa fa-calendar fa-fw"></i> <?php echo $eventrow['eventtitle'];?>
                                    <!-- <span class="pull-right text-muted small"><?php echo $eventrow['eventdate'];?></span> -->
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <?php }?>
                        <li>
                            <a class="text-center" href="events.php">
                                <strong>See All Events</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="mailto:info@mypubspace.com"><i class="fa fa-user fa-fw"></i> Contact Admin</a>
                        </li>
                        <!-- 
<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
 -->
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!-- 
<li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!~~ /input-group ~~>
                        </li>
 -->

                        <li>
                            <a href="main.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a> 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-calendar-o fa-fw"></i> Events<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="events.php">View Events</a>
                                </li>
                                <li>
                                    <a href="addevent.php">Add Event</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-beer-o fa-fw"></i> Pubs &amp; Venues<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="pubs.php">View Pubs/Venues</a>
                                </li>
                                <li>
                                    <a href="addpub.php">Add Pub/Venue</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            
            
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                    <h3>You are an administrator for the <?php echo $myTown;?> area.</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="col-md-12">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-calendar fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo mysqli_num_rows($events);?></div>
                                    <div>Events</div>
                                </div>
                            </div>
                        </div>
                        <a href="events.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Events</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline">
							
							<thead>
							<?php if ($myTown == 'ALL') {?>
								<tr>
									<td colspan="8">
										<div id="tabs">
											<ul>
												<li><a href="#tabs-1">Search</a></li>
												<li><a href="#tabs-2">Filter search</a></li>
												<li><a href="#tabs-3">Search between dates</a></li>
												<li><a href="#tabs-4">Advanced search</a></li>
											</ul>
											<div id="tabs-1">
												<form action="pubs-search.php" method="post">
												<label for="search" class="inline"></label>
												<select name="mode">
													<option value="FirstName">First Name:</option>
													<option value="rsPubName">Last Name:</option>
													<option value="rsTown">Company:</option>
													<option value="rsCounty">Discount Code:</option>
												</select>
												<input type="text" class="text-input small-input" id="searchstring" name="searchstring"/>
												<input class="button" type="submit" value="Search" />
												</form>
											</div>
											<div id="tabs-2">
												<form name="form3" method="post" action="">
												<label for="filter" class="inline">Search: </label>
												<select name="rsTown" onChange="MM_jumpMenu('parent',this,0)">
													<option value="">Town</option>
												<?php while($row1 = mysql_fetch_array($result3))
												{
													echo '<option value="pubs-filter.php?rsTown='.$row1['rsTown'].'">'.$row1['rsTown'].'</option>';
												}
												?>
												</select>
												<label for="filter" class="inline">Search: </label>
												<select name="rsCounty" onChange="MM_jumpMenu('parent',this,0)">
													<option value="">County</option>
												<?php while($row1 = mysql_fetch_array($result4))
												{
													echo '<option value="pubs-filter.php?rsCounty='.$row1['rsCounty'].'">'.$row1['rsCounty'].'</option>';
												}
												?>
												</select>
												</form>
											</div>
											<div id="tabs-3">
												<form action="pubs-by-dates.php" method="post">
												<label for="from" class="inline">From: </label>
												<input type="text" class="text-input smaller-input" id="from" name="from"/>
												<label for="to" class="inline">To: </label>
												<input type="text" class="text-input smaller-input" id="to" name="to"/>
												<input class="button" type="submit" value="Search dates" />
												</form>
											</div>
											<div id="tabs-4">
												<form action="pubs-advanced.php" method="post">
												<label for="filter" class="inline">Search: </label>
												<select name="rsTown">
													<option value="">Town Search</option>
												<?php while($row1 = mysql_fetch_array($result6))
												{
													echo '<option value="'.$row1['rsTown'].'">'.$row1['rsTown'].'</option>';
												}
												?>
												</select>
												<select name="rsCounty">
													<option value="">County Search</option>
												<?php while($row1 = mysql_fetch_array($result7))
												{
													echo '<option value="'.$row1['rsCounty'].'">'.$row1['rsCounty'].'</option>';
												}
												?>
												</select>
												<label for="from" class="inline">From: </label>
												<input type="text" class="text-input smaller-input" id="from1" name="from1"/>
												<label for="to" class="inline">To: </label>
												<input type="text" class="text-input smaller-input" id="to1" name="to1"/>
												<input class="button" type="submit" value="Search" />
												</form>
											</div>
										</div>
									</td>
								</tr>
								<?php }?>
								<tr>
								   <th>Pub ID <a href="pubs.php?sortby=PUBID&sorting=DESC">&darr;</a><a href="pubs.php?sortby=PUBID&sorting=ASC">&uarr;</a></th>
								   <th>Pub Name</th>
								   <th>Town</th>
								   <th>&nbsp;</th>
								   <th>&nbsp;</th>
								   <th>County <a href="pubs.php?sortby=rsCounty&sorting=DESC">&darr;</a><a href="pubs.php?sortby=rsCounty&sorting=ASC">&uarr;</a></th>
								   <th>&nbsp;</th>
								   <th>&nbsp;</th>
								</tr>
								
							</thead>
						 
							<tfoot>
								<tr>
									<td colspan="8">
										<!-- .pagination -->
										<?php echo $paginatecust;?>
										<!-- End .pagination -->
										<div class="clear"></div>
									</td>
								</tr>
							</tfoot>
						 
							<tbody>
								<?php 
								while($row1 = mysqli_fetch_array($result)) 
									{
										echo '<tr>';
										echo '<td><a href="view-pub.php?PUBID='.$row1['PUBID'].'" title="PUBID">'.$row1['PUBID'].'</a></td>'; 
										echo '<td>'.$row1['rsPubName'].'</td>'; 
										echo '<td>'.$row1['rsTown'].'</td>';
										echo '<td>&nbsp;</td>';
										echo '<td>&nbsp;</td>';
										echo '<td>'.$row1['rsCounty'].'</td>';
										echo '<td>&nbsp;</td>';
										echo '<td><a class="btn btn-warning btn-circle btn-md" href="edit-pub.php?PUBID='.$row1['PUBID'].'" title="Edit"><i class="fa fa-edit"/></a></td>';
										echo '</tr>';
										
									}
								?>
							</tbody>
							<tr>
								<td colspan="2"></td>
								<td>pubs: <strong><?php echo $total_pages;?></strong></td>
								<td colspan="5"></td>
							</tr>
								
						</table>
                </div>
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
