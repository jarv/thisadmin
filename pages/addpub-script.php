<?php
include_once("config.php");
// if (!isset($_SESSION['rsUser'])) {
// $msg = "Username and/or Password incorrect!";
// header('Location: index.php?msg='.$msg.'');
// }

$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
$path = 'uploads/'; // upload directory

if(isset($_FILES['img1']))
{
 $img = $_FILES['img1']['name'];
 $tmp = $_FILES['img1']['tmp_name'];
  
 // get uploaded file's extension
 $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
 
 // can upload same image using rand function
 $final_image = rand(1000,1000000).$img;
 
 // check's valid format
 if(in_array($ext, $valid_extensions)) 
 {     
  $path = $path.strtolower($final_image); 
   
  if(move_uploaded_file($tmp,$path)) 
  {
   //echo "<img src='$path' />";
  }
 } 
 else 
 {
  $path = 'uploads/icon_places.svg';
 }
}


 $PUBID = str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT);
 $rsPubName = mysqli_real_escape_string($link,stripslashes($_POST['rsPubName']));
 $rsAddress = mysqli_real_escape_string($link,stripslashes($_POST['address']));
 $Add2 = mysqli_real_escape_string($link,stripslashes($_POST['add2']));
 $rsTown = mysqli_real_escape_string($link,stripslashes($_POST['rsTown']));
 $rsCounty = mysqli_real_escape_string($link,stripslashes($_POST['rsCounty']));
 $rsPostCode = mysqli_real_escape_string($link,stripslashes($_POST['rsPostCode']));
 $Region = mysqli_real_escape_string($link,stripslashes($_POST['region']));
 $PremisesType = mysqli_real_escape_string($link,stripslashes($_POST['PremisesType']));
 $rsTel = mysqli_real_escape_string($link,stripslashes($_POST['rsTel']));
 $rsWebsite = mysqli_real_escape_string($link,stripslashes($_POST['rsWebsite']));
 $rsLat = substr($_POST['rsLat'], 0, 10);
 $rsLong = substr($_POST['rsLong'], 0, 10);
 $rsAboutpub = mysqli_real_escape_string($link,stripslashes($_POST['rsAboutpub']));
 $offer1 = mysqli_real_escape_string($link,stripslashes($_POST['offer1'])); 


$sql = "INSERT INTO pubs (PUBID,rsPubName,rsAddress,Add2,rsTown,rsCounty,rsPostCode,Region,PremisesType,rsTel,rsWebsite,rsLat,rsLong,rsAboutPub,img1,offer1)
VALUES ('$PUBID','$rsPubName','$rsAddress','$Add2','$rsTown','$rsCounty','$rsPostCode','$Region','$PremisesType','$rsTel','$rsWebsite','$rsLat','$rsLong','$rsAboutpub','$path','$offer1')";

//echo $sql;
$msg = "Venue added";
$result = mysqli_query($link,$sql) or die('Error: ' . mysql_error() . '<br>SQL: ' . $sql);
header("Location: /pages/pubs.php?msg=$msg"); 
mysqli_close($link);
?> 