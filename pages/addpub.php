<?php
error_reporting( E_ALL | E_STRICT );
ini_set('display_errors', 1);
session_start();
include_once("config.php");
$myTown = $_SESSION['rsTown'];
if($_SESSION['rsUser'] == 'admin'){
	$myTown = 'London';
}
if (!isset($_SESSION['rsUser'])) {
$msg = "Username and/or Password incorrect!";
header('Location: login.php?msg='.$msg.'');
}

$sql2 = "SELECT DISTINCT rsTown, rsCounty FROM pubs WHERE rsTown = '$myTown'";
$row1 = mysqli_fetch_array(mysqli_query($link,$sql2));

$sql = "SELECT DISTINCT rsTown FROM pubs";
$town = mysqli_query($link,$sql);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ThisAdmin</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    
    <!-- Datetimepicker CSS -->
    <link href="../dist/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
	<a name="top"></a>
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">ThisAdmin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <?php 
                $eventsquery = "SELECT * FROM pubs INNER JOIN pubs_events ON pubs_events.eventvenue=pubs.PUBID WHERE pubs.rsTown = '$myTown' ORDER BY eventdate ASC LIMIT 0,10";
                $events = mysqli_query($link,$eventsquery);
                ?>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-calendar fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                    <?php while($eventrow = mysql_fetch_array($events)) {?>
                        <li>
                            <a href="viewevent.php?eventid=<?php echo $eventrow['eventid'];?>">
                                <div>
                                    <i class="fa fa-calendar fa-fw"></i> <?php echo $eventrow['eventtitle'];?>
                                    <!-- <span class="pull-right text-muted small"><?php echo $eventrow['eventdate'];?></span> -->
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <?php }?>
                        <li>
                            <a class="text-center" href="events.php">
                                <strong>See All Events</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="mailto:info@mypubspace.com"><i class="fa fa-user fa-fw"></i> Contact Admin</a>
                        </li>
                        <!-- 
<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
 -->
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!-- 
<li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!~~ /input-group ~~>
                        </li>
 -->

                        <li>
                            <a href="main.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a> 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-calendar-o fa-fw"></i> Events<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="events.php">View Events</a>
                                </li>
                                <li>
                                    <a href="addevent.php">Add Event</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-beer-o fa-fw"></i> Pubs &amp; Venues<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="pubs.php">View Pubs/Venues</a>
                                </li>
                                <li>
                                    <a href="addpub.php">Add Pub/Venue</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            
            
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Venue</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-calendar fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"></div>
                                    <div>Events</div>
                                </div>
                            </div>
                        </div>
                        <a href="events.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Events</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                
                <div class="col-md-12">
                	<form id="form" action="addpub-script.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Venue Name</label>
                        <input class="form-control" id="rsPubName" name="rsPubName">
                    </div>
                    <div class="form-group">
						<div id="myMap"></div>
						<label>Address</label>
						<input id="address" class="form-control" name="address" type="text"/>
						<input type="hidden" name="rsLat" id="rsLat" placeholder="Latitude"/>
						<input type="hidden" name="rsLong" id="rsLong" placeholder="Longitude"/>
					</div>
					<div class="form-group">
                        <label>Address (line 2)</label>
                        <input class="form-control" id="add2" name="add2">
                    </div>
					<div class="form-group">
                        <label>Town</label>
						<select name="rsTown" id="rsTown" class="form-control">
							<?php while($row = mysqli_fetch_array($town)) { ?>
								<option value="<?php echo $row['rsTown']; ?>" <?php if($row['rsTown']==$row1['rsTown']){ echo ' selected'; }?>><?php echo $row['rsTown']; ?></option>
							<? }?>
						</select>
                    </div>
					<div class="form-group">
                        <label>County</label>
					<?php if($row1['rsCounty'] != ""){?>
								<input class="form-control" readonly id="rsCounty"  name="rsCounty" id="rsCounty" value="<?php echo $row1['rsCounty']; ?>">
								<?php }?>
                    </div>
                    <div id="rsCounty"></div>
					<div class="form-group">
                        <label>Post Code</label>
                        <input class="form-control" id="rsPostCode" name="rsPostCode">
                    </div>
					<div class="form-group">
                        <label>Region</label>
                        <input id="region" class="form-control" name="region" type="text"/>
                    </div>
					<div class="form-group">
                        <label>Premises Type</label>
						<select name="PremisesType" id="PremisesType" class="form-control">
                        	<option value="Pub">Pub</option>
                        	<option value="Theatre">Theatre</option>
                        	<option value="Concert Hall">Concert Hall</option>
                        	<option value="Cafe">Cafe</option>
                        	<option value="Hall">Hall</option>
                        	<option value="Church">Church</option>
                        	<option value="Outdoor">Outdoor</option>
                        	<option value="School">School</option>
                        </select>
                    </div>
					<div class="form-group">
                        <label>Telephone</label>
                        <input class="form-control" id="rsTel" name="rsTel">
                    </div>
					<div class="form-group">
                        <label>Website</label>
                        <input class="form-control" id="rsWebsite" name="rsWebsite">
                    </div>
					<div class="form-group">
                        <label>Description</label>
                        <input class="form-control" id="rsAboutpub" name="rsAboutpub">
                    </div>
					<div class="form-group">
                        <label>Img 1 (URL)</label>
    					<input type="file" accept="image/*" class="form-control" id="img1" name="img1" />
                    </div>
					<div class="form-group">
                        <label>Offer Code</label>
                        <input class="form-control" id="offer1" name="offer1">
                    </div>
                    <div class="form-group">
                    	<div id="result"></div>
                    </div>
                    <div class="form-group">
                    	<button id="savebutton" class="btn btn-primary btn-lg"><i class="fa fa-plus" aria-hidden="true"></i> Add venue</button>
                    </div>
					<div class="form-group">
                    	<!-- <button class="btn btn-info btn-circle btn-lg" type="submit"><i class="fa fa-check"></i></button> -->
                    	<!-- <a href="#" class="btn btn-warning btn-circle btn-lg" role="button"><i class="fa fa-times"></i></a> -->
                    </div>  
                    </form>           
                </div>
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    
    <!-- moment JavaScript -->
    <script src="../dist/js/moment.js"></script>
    <script src="../dist/js/Moment.min.js"></script>
    <!-- datetimepicker JavaScript -->
    <script src="../dist/js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript">
            
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
        </script>
        <script>
$( document ).ready(function() {
    console.log( "ready!" );

$("#rsTown").change(function() {
    //in here we can do the ajax after validating the field isn't empty.
    if($("#rsTown").val()!="") {
        $.ajax({
            url: "http://www.mypubspace.com/pubsmobile/getcounties.php",
            type: "POST",
            async: true, 
            data: { town:$("#rsTown").val() }, //your form data to post goes here as a json object
            dataType: "html",

            success: function(data) {
                $('#rsCounty').val(data);    
            },  
        });
    } else {
        //notify the user they need to enter data
    }
});
});
</script>

<script>
$(document).ready(function (event) {
$("#form").on('submit',(function(event) {
    event.preventDefault();
    var val1 = $('#rsPubName').val();
    var val2 = $('#address').val();
    var val3 = $('#rsLat').val();
    var val4 = $('#rsLong').val();
    var val5 = $('#add2').val();
    var val6 = $('#rsTown').val();
    var val7 = $('#rsCounty').val();
    var val8 = $('#rsPostCode').val();
    var val9 = $('#region').val();
    var val10 = $('#PremisesType').val();
    var val11 = $('#rsTel').val();
    var val12 = $('#rsWebsite').val();
    var val13 = $('#rsAboutpub').val();
    var val14 = $('#img1').val();
    var val15 = $('#offer1').val();
    $.ajax({
        type: 'POST',
        url: 'addpub-script.php',
        cache: false,
	    contentType: false,
    	processData: false,
        data: { rsPubName: val1, address: val2, rsLat: val3, rsLong: val4, add2: val5, rsTown: val6, rsCounty: val7, rsPostCode: val8, region: val9, PremisesType: val10, rsTel: val11, rsWebsite: val12, rsAboutpub: val13, img1: val14, offer1: val15},
        success: function(response) {
            $('#result').html( '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>New venue added!</strong> Why not <a href="#top" class="alert-link">add another?</a></div>').fadeIn()
        }
    });
});
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false">
</script>

<script type="text/javascript"> 
var map;
var marker;
var myLatlng = new google.maps.LatLng(51.502268,-0.1396042);
var geocoder = new google.maps.Geocoder();
var infowindow = new google.maps.InfoWindow();
function initialize(){
var mapOptions = {
zoom: 12,
center: myLatlng,
mapTypeId: google.maps.MapTypeId.ROADMAP
};

map = new google.maps.Map(document.getElementById("myMap"), mapOptions);

marker = new google.maps.Marker({
map: map,
position: myLatlng,
draggable: true 
}); 

geocoder.geocode({'latLng': myLatlng }, function(results, status) {
if (status == google.maps.GeocoderStatus.OK) {
if (results[0]) {
$('#rsLat,#rsLong').show();
$('#address').val(results[0].formatted_address);
$('#address6').val(results[6].formatted_address);
$('#latitude').val(marker.getPosition().lat());
$('#longitude').val(marker.getPosition().lng());
infowindow.setContent(results[0].formatted_address);
infowindow.open(map, marker);
}
}
});

google.maps.event.addListener(marker, 'dragend', function() {

geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
if (status == google.maps.GeocoderStatus.OK) {
if (results[0]) {
$('#address').val(results[0].formatted_address);
$('#address6').val(results[6].formatted_address);
$('#rsLat').val(marker.getPosition().lat());
$('#rsLong').val(marker.getPosition().lng());
infowindow.setContent(results[0].formatted_address);
infowindow.open(map, marker);
}
}
});
});

}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
</body>
</html>