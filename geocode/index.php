<?php
include_once('db.php');
ini_set('display_errors', 1);
error_reporting(E_ALL);

/** START CONFIG */

$host = 'db643632377.db.1and1.com';
$username = 'dbo643632377';
$password = 'Scaramanga79';
$dbName = 'db643632377';

$startPostcode = 'BN11 8YJ';
$table = 'southdowns2';

define('DEBUG_STATUS', true);

/** END CONFIG*/


debug('Start');

$dbConn = db( 'default', $host, $username, $password, $dbName );

debug('Connected');
debug('');

$result = $dbConn->query( "SELECT * FROM ".$table." WHERE miles IS NULL OR miles < 1 LIMIT 20000" );
$results = $result->all(RESULT_AS_LIST);
$startLatLong = PlodCalculation::getLatLong($startPostcode);
$i=1;
foreach($results as $result){
    $i++;

    if($i % 20 == 0){
        //sleep to stop API calls happening too frequently
        sleep(1);
    }

    $rowId = $result['id'];
    $postcode = $result['postcode'];

    $distance = PlodCalculation::getDistance($startPostcode, $postcode, $startLatLong);
    $latlon = PlodCalculation::getLatLong($postcode);
    $lat = $latlon['lat'];
    $lon = $latlon['lon'];
    if($distance){
        $update = 'UPDATE ' . $table . ' SET miles = "' . $distance['distance_miles'] . '", drive_time = "' . $distance['time'] . '", lat = "' . $latlon['lat'] . '", lon = "' . $latlon['lon'] . '" WHERE id = ' . $rowId;
        debug($update);
        $dbConn->query($update);
    }

    debug('');

}

debug('Finished');

function debug($string, $data = null){
    if(DEBUG_STATUS){
        echo $string;
        if(!is_null($data)){
            echo ': ';
            if(is_string($data)){
                echo $data;
            }else{
                echo '<pre>';
                var_dump($data);
                echo '</pre>';
            }
        }
        echo '<br/>';
    }
}

/**
 * Class PlodCalculation
 * @author Mark Wallman <mark.wallman@gmail.com>
 */
class PlodCalculation
{
  const API_KEY = 'AIzaSyA9LEIiRbXr0p-fb8NhdTf6wexm0coZZoU';
  //const API_KEY = 'AIzaSyDiiXgFNvsSIQueVJkl8HIoOhXoqimxolU';
  //const API_KEY = 'AIzaSyBJ9NnnBrCgoaJTww23lj4BkY2JWxdgNwM';
    protected $dbConn;

    /**
     * @param $km
     * @return string
     */
    protected static function kmToMiles($km)
    {
        $milesConv= 0.621;
        $miles = $km * $milesConv;

        return $miles;
    }

    /**
     * @param $startPostcode
     * @param $endPostcode
     * @param bool $startLatLong
     * @return array
     */
    public static function getDistance($startPostcode, $endPostcode, $startLatLong = false)
    {
        debug(__CLASS__.'::'.__FUNCTION__, 'Postcode: '. $endPostcode);

        if(!$startLatLong){
            $startLatLong = self::getLatLong($startPostcode);
        }

        $endLatLong = self::getLatLong($endPostcode);

        if(!$startLatLong || !$endLatLong){
            debug('API failed to calculate lat long');
            //failed to calculate lat long
            return false;
        }

        $distance = self::getDrivingDistance($startLatLong['lat'], $endLatLong['lat'], $startLatLong['lon'], $endLatLong['lon']);

        debug('Distance', $distance);
        return $distance;
    }

    public static function getDrivingDistance($lat1, $lat2, $long1, $long2)
    {
        //$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&key=" . self::API_KEY;

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&key=" . self::API_KEY;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);

        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
        $distMiles = round(self::kmToMiles(str_replace('km', '', $dist)),2);

        return array('distance_km' => $dist, 'distance_miles' => $distMiles, 'time' => $time);
    }

    /**
     * @param $postcode
     * @return array|bool
     */
    public static function getLatLong($postcode)
    {
        $postcode.= ', UK';

        $request_url = "https://maps.googleapis.com/maps/api/geocode/xml?address=".$postcode."&sensor=true&key=" . self::API_KEY;
        $xml = simplexml_load_file($request_url) or die("url not loading");
        $status = $xml->status;
        if ($status=="OK") {
            $Lat = $xml->result->geometry->location->lat;
            $Lon = $xml->result->geometry->location->lng;
            return array('lat' => (string)$Lat, 'lon' => (string)$Lon);
        }

        return false;
    }

}
?>
