<?php include("config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PLODS</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
body,td,th {
	color: #333;
}
.contents{
	margin: 20px;
	padding: 20px;
	list-style: none;
	background: #F9F9F9;
	border: 1px solid #ddd;
	border-radius: 5px;
}
span.numberofpubs {
	font-size: 34px;
	text-align:right;
}
.pub-listing {
	min-height:300px;
}
.contents li{
    margin-bottom: 10px;
}
.loading-div{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.56);
	z-index: 999;
	display:none;
}
.loading-div img {
	margin-top: 20%;
	margin-left: 50%;
}

/* Pagination style */

.pagination li.active{
	    position: relative;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #DDDDDD;
    border: 1px solid #ddd;
}
.pagination>li {
    display: INLINE-FLEX;
}
      #map {
        width: 100%;
        height: 400px;
        background-color: grey;
      }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <?php

    $mysqli = mysqli_connect("db623717149.db.1and1.com", "dbo623717149", "Scaramanga79", "db623717149");
    //$sql_locations = "SELECT CONCAT(name1, ' ', name2, ' of ', address1, ', ', address2) as address, Latitude, Longitude FROM southdowns";
    $sql_locations = "SELECT CONCAT(name1, ' ', name2, ' of ', address1, ', ', address2) as address, Latitude, Longitude FROM southdowns";
    $rs_locations = mysqli_query($mysqli, $sql_locations);

    while( $rs_location = $rs_locations->fetch_row() ) {
        $markers[] = $rs_location;
    }
    //while( $rs_location = $rs_locations->fetch_assoc() ) {
    //    $markers[] = $rs_location;
    //}

    //echo "<pre>".print_r($markers)."</pre>";
?>
  </head>
  <body>
  	<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PLODS</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="./">Default <span class="sr-only">(current)</span></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    <div class="container">
	<?php
  //echo json_encode( $markers );
	//Limit our results within a specified range.
	$results = $mysqli->query("SELECT DISTINCT event_name FROM southdowns");
	$theevent = $results->fetch_row();
	echo '<h1>'.$theevent[0].'</h1>'; ?>
    
    <p>
  <label for="amount">Distance (meters):</label>
  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
</p>
<div id="slider-range-max"></div>
	  <div class="loading-div"><img src="ajax-loader.gif" ></div>
	  <div id="results"><!-- content will be loaded here --></div>

		</div>
    </div> <!-- /container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
  $( function() {
    $( "#slider-range-max" ).slider({
      range: "max",
      min: 1,
      max: 160934,
      value: 16093,
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.value );
      }
    });
    $( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );
  } );
  </script>
	<script type="text/javascript">
$(document).ready(function() {
	$("#results" ).load( "fetch_map_markers.php"); //load initial records
	$("#towns" ).load( "fetch_towns.php"); //load initial records

	//executes code below when user click on pagination links
	$("#results").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#results").load("fetch_pages.php",{"page":page}, function(){ //get content from PHP page
			$(".loading-div").hide(); //once done, hide loading element
		});

	});
	$('select[name="towns"]').change(function() {
		var town = $('select[name="towns"]').val();
		$("#results" ).load( "fetch_pages.php?rsTown="+town);
	});
});
</script>
<script>

    

        // Multiple Markers
        var markers = <?php echo json_encode( $markers ); ?>;

    var map = null;
      var radius_circle = null;
      var markers_on_map = [];
      
      //all_locations is just a sample, you will probably load those from database
      var all_locations = [
        {type: "Restaurant", name: "Restaurant 1", lat: 40.723080, lng: -73.984340},
        {type: "School", name: "School 1", lat: 40.724705, lng: -73.986611},
        {type: "School", name: "School 2", lat: 40.724165, lng: -73.983883},
        {type: "Restaurant", name: "Restaurant 2", lat: 40.721819, lng: -73.991358},
        {type: "School", name: "School 3", lat: 40.732056, lng: -73.998683}
      ];

      //initialize map on document ready
      $(document).ready(function(){
          var latlng = new google.maps.LatLng(40.723080, -73.984340); //you can use any location as center on map startup
          var myOptions = {
            zoom: 14,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          map = new google.maps.Map(document.getElementById("map"), myOptions);
        
        google.maps.event.addListener(map, 'click', showCloseLocations);
      });
      
      function showCloseLocations(e) {
        var i;
        var radius_km = $('#radius_km').val();
        var address = $('#address').val();

        //remove all radii and markers from map before displaying new ones
        if (radius_circle) {
          radius_circle.setMap(null);
          radius_circle = null;
        }
        for (i = 0; i < markers_on_map.length; i++) {
          if (markers_on_map[i]) {
            markers_on_map[i].setMap(null);
            markers_on_map[i] = null;
          }
        }
      
        var address_lat_lng = e.latLng;
        radius_circle = new google.maps.Circle({
          center: address_lat_lng,
          radius: radius_km * 1000,
          clickable: false,
          map: map
        });
      if(radius_circle) map.fitBounds(radius_circle.getBounds());
        for (var j = 0; j < all_locations.length; j++) {
          (function (location) {
            var marker_lat_lng = new google.maps.LatLng(location.lat, location.lng);
            var distance_from_location = google.maps.geometry.spherical.computeDistanceBetween(address_lat_lng, marker_lat_lng); //distance in meters between your location and the marker
            if (distance_from_location <= radius_km * 1000) {
              var new_marker = new google.maps.Marker({
                position: marker_lat_lng,
                map: map,
                title: location.name
              });
              google.maps.event.addListener(new_marker, 'click', function () {
                alert(location.name + " is " + distance_from_location + " meters from my location");
              });
              markers_on_map.push(new_marker);
            }
          })(all_locations[j]);
        }
      }
</script>
<!-- language: lang-html -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKZKPTHQYs9ryRUFtMUUYXJoWty1l7WKY">
</script>
      <script type="text/javascript">
        google.load("maps", "3",{other_params:"sensor=false&libraries=geometry"});
      </script>

<body style="margin:0px; padding:0px;" >
 
 <select id="radius_km">
   <option value=1>1km</option>
   <option value=2>2km</option>
   <option value=5>5km</option>
   <option value=30>30km</option>
 </select>
 <div id="map"  style="width:500px; height:300px;">
    </body>
</html>
