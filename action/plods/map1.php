<?php include("config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PLODS</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
body,td,th {
	color: #333;
}
.contents{
	margin: 20px;
	padding: 20px;
	list-style: none;
	background: #F9F9F9;
	border: 1px solid #ddd;
	border-radius: 5px;
}
span.numberofpubs {
	font-size: 34px;
	text-align:right;
}
.pub-listing {
	min-height:300px;
}
.contents li{
    margin-bottom: 10px;
}
.loading-div{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.56);
	z-index: 999;
	display:none;
}
.loading-div img {
	margin-top: 20%;
	margin-left: 50%;
}

/* Pagination style */

.pagination li.active{
	    position: relative;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #DDDDDD;
    border: 1px solid #ddd;
}
.pagination>li {
    display: INLINE-FLEX;
}
      #map {
        width: 100%;
        height: 400px;
        background-color: grey;
      }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <?php

    $mysqli = mysqli_connect("db623717149.db.1and1.com", "dbo623717149", "Scaramanga79", "db623717149");
    //$sql_locations = "SELECT CONCAT(name1, ' ', name2, ' of ', address1, ', ', address2) as address, Latitude, Longitude FROM southdowns";
    $sql_locations = "SELECT CONCAT(name1, ' ', name2) as address, Latitude, Longitude FROM southdowns";
    $rs_locations = mysqli_query($mysqli, $sql_locations);

    while( $rs_location = $rs_locations->fetch_row() ) {
        $markers[] = $rs_location;
    }
    //while( $rs_location = $rs_locations->fetch_assoc() ) {
    //    $markers[] = $rs_location;
    //}

    //echo "<pre>".print_r($markers)."</pre>";
?>
  </head>
  <body>
  	<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PLODS</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="./">Default <span class="sr-only">(current)</span></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    <div class="container">
	<?php
  //echo json_encode( $markers );
	//Limit our results within a specified range.
	$results = $mysqli->query("SELECT DISTINCT event_name FROM southdowns");
	$theevent = $results->fetch_row();
	echo '<h1>'.$theevent[0].'</h1>'; ?>
    <div id="map"></div>
    <p>
  <label for="amount">Distance (meters):</label>
  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
</p>
<div id="slider-range-max"></div>
	  <div class="loading-div"><img src="ajax-loader.gif" ></div>
	  <div id="results"><!-- content will be loaded here --></div>

		</div>
    </div> <!-- /container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKZKPTHQYs9ryRUFtMUUYXJoWty1l7WKY">
</script>
    <script src="js/maplace.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
  $( function() {
    $( "#slider-range-max" ).slider({
      range: "max",
      min: 1,
      max: 160934,
      value: 16093,
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.value );
      }
    });
    $( "#amount" ).val( $( "#slider-range-max" ).slider( "value" ) );
  } );
  </script>
	<script type="text/javascript">
$(document).ready(function() {
	$("#results" ).load( "fetch_map_markers.php"); //load initial records
	$("#towns" ).load( "fetch_towns.php"); //load initial records

	//executes code below when user click on pagination links
	$("#results").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#results").load("fetch_pages.php",{"page":page}, function(){ //get content from PHP page
			$(".loading-div").hide(); //once done, hide loading element
		});

	});
	$('select[name="towns"]').change(function() {
		var town = $('select[name="towns"]').val();
		$("#results" ).load( "fetch_pages.php?rsTown="+town);
	});
});
</script>
<script>

    function initialize() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: "roadmap",
            center: new google.maps.LatLng(52.791331, -1.918728), // somewhere in the uk BEWARE center is required
            zoom: 14,
        };

        // Display a map on the page
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        map.setTilt(45);

        // Multiple Markers
        var markers = <?php echo json_encode( $markers ); ?>;

        // Display multiple markers on a map
        var infoWindow = new google.maps.InfoWindow();
        var marker, i;

        var Circle = [
            {
                lat: 51.51328,
                lon: -0.09021,
                circle_options: {
                    radius: 160,
                    editable: true
                },
                title: 'Editable circle',
                html: 'Change my size',
                visible: false,
                draggable: true
            }
        ];
        new Maplace({
            locations: Circle,
            map_div: '#map',
            start: 4,
            view_all_text: 'Points of interest',
            type: 'circle',
            shared: {
                zoom: 16,
                html: '%index'
            },
            circleRadiusChanged: function(index, point, marker) {
                $('#radiusInfo').text(
                    ' - point #' + (index+1) + ' size: ' + parseInt(marker.getRadius()) + 'mt.'
                );
            }
        }).Load();
        // Loop through our array of markers & place each one on the map
        for (i = 0; i < markers.length; i++) {
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: markers[i][0]
            });

            // Allow each marker to have an info window
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infoWindow.setContent(markers[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        }

        // Add circle overlay and bind to marker
        //var circle = new google.maps.Circle({
        //  map: map,
        //  radius: 16093,    // 10 miles in metres
        //  fillColor: '#AA0000'
        //});
        //circle.bindTo('center', marker, 'position');


        //Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
            this.setZoom(7);
            google.maps.event.removeListener(boundsListener);
        });

        

    }
    
    google.maps.event.addDomListener(window, 'load', initialize);



</script>

  </body>
</html>
