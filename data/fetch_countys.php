<?php

//continue only if $_POST is set and it is a Ajax request
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
	include("config.inc.php");  //include config file
	//Get page number from Ajax POST
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
		$page_number = 1; //if there's no page number, set it to 1
	}
	
	//get total number of records from database for pagination
	$results = $mysqli->query("SELECT DISTINCT rsCounty COUNT(*) FROM pubs");
	$get_total_rows = $results->fetch_row(); //hold total records in variable
	//break records into pages
	$total_pages = ceil($get_total_rows[0]/$item_per_page);
	
	//get starting position to fetch the records
	$page_position = (($page_number-1) * $item_per_page);
	

	//Limit our results within a specified range. 
	$results = $mysqli->prepare("SELECT DISTINCT rsCounty FROM pubs ORDER BY rsTown");
	$results->execute(); //Execute prepared Query	
$results->bind_param('s',$rsCounty);
$results->bind_result($rsCounty); //bind variables to prepared statement
	
	//Display records fetched from database.
	echo '<div class="row">';
	echo '<div class="form-group">';
	echo '<label for="sel1">Select County:</label>'

	echo '<select name="counties" class="countieslist form-control">';
	while($results->fetch()){ //fetch values
	
		echo '<option value="'.$rsCounty.'">'.$rsCounty.'</option>';
	
	}
	
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '<div align="center">';
	/* We call the pagination function here to generate Pagination link for us. 
	As you can see I have passed several parameters to the function. */
	echo '</div>';
	
	exit;
}


?>

