<?php include("config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PLODS</title>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.js"></script>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
body,td,th {
	color: #333;
}
.contents{
	margin: 20px;
	padding: 20px;
	list-style: none;
	background: #F9F9F9;
	border: 1px solid #ddd;
	border-radius: 5px;
}
span.numberofpubs {
	font-size: 34px;
	text-align:right;
}
.pub-listing {
	min-height:300px;
}
.contents li{
    margin-bottom: 10px;
}
.loading-div{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.56);
	z-index: 999;
	display:none;
}
.loading-div img {
	margin-top: 20%;
	margin-left: 50%;
}

/* Pagination style */

.pagination li.active{
	    position: relative;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #DDDDDD;
    border: 1px solid #ddd;
}
.pagination>li {
    display: INLINE-FLEX;
}

</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&key=AIzaSyDiiXgFNvsSIQueVJkl8HIoOhXoqimxolU&ext=.js"></script>



<style type="text/css">
html, body, #map_canvas {
height: 100%;
width: 100%;
top:0px;
left:0px;
}

</style>
  </head>
  <body>
  	<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PLODS</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="./">Default <span class="sr-only">(current)</span></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    <div class="container">
	<?php
	//Limit our results within a specified range.
	//$results = $mysqli->query("SELECT DISTINCT event_name FROM cotswolds");
	//$theevent = $results->fetch_row();
	echo '<h1>Potential Registrants</h1>'; ?>
	  <div class="loading-div"><img src="ajax-loader.gif" ></div>
	  <div id="results"><!-- content will be loaded here --></div>

		</div>
    </div> <!-- /container -->

    <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script type="text/javascript">
$(document).ready(function() {

	//executes code below when user click on pagination links
	$("#results").on( "click", ".pagination a", function (e){
		e.preventDefault();
		$(".loading-div").show(); //show loading element
		var page = $(this).attr("data-page"); //get page number from link
		$("#results").load("fetch_pages.php",{"page":page}, function(){ //get content from PHP page
			$(".loading-div").hide(); //once done, hide loading element
		});

	});
	$('select[name="towns"]').change(function() {
		var town = $('select[name="towns"]').val();
		$("#results" ).load( "fetch_pages.php?rsTown="+town);
	});
});
</script>
<!--<ul class="citylist">
  <li><a href="#">BN14 7HX</a></li>
  <li><a href="#">RH12 2DP</a></li>
  <li><a href="#">BN15 8AR</a></li>
  <li><a href="#">Harrogate</a></li>
</ul>-->
<div id="map_canvas"></div>

<?php

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

// Report all PHP errors (see changelog)
error_reporting(E_ALL);
//continue only if $_POST is set and it is a Ajax request
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

	
	//Get page number from Ajax POST
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
		$page_number = 1; //if there's no page number, set it to 1
	}

	//get total number of records from database for pagination
		$results = $mysqli->query("SELECT COUNT(*) FROM cotswolds");

	$get_total_rows = $results->fetch_row(); //hold total records in variable
	//break records into pages
	$total_pages = ceil($get_total_rows[0]/$item_per_page);

	//get starting position to fetch the records
	$page_position = (($page_number-1) * $item_per_page);



	//Limit our results within a specified range.

		$results = $mysqli->prepare("SELECT 	id,	CompanyID,	Title,	FirstName,	Surname,	JobTitle,	Email,	CompanyName,	Address1,	Address2,	Address3,	Town,	County,	postcode,	Telephone,	VerticalMarket,	EmployeesCompany,	Turnover,	LocationIndicator,	Region,	drive_time,	miles,	miles_sd,	drive_time_sd FROM cotswolds ORDER BY Surname ASC LIMIT $page_position, $item_per_page");
	$results->execute(); //Execute prepared Query

$results->bind_result($id, $CompanyID, $Title, $FirstName, $Surname, $JobTitle,	$Email,	$CompanyName,	$Address1, $Address2,	$Address3, $Town, $County, $postcode,	$TelephoneNumber,	$VerticalMarket, $EmployeesCompany,	$Turnover, $LocationIndicator, $Region, $drive_time, $miles, $miles_sd,	$drive_time_sd ); //bind variables to prepared statement
	echo '</div>';
	//Display records fetched from database.
	echo '<div class="row">';
	echo '<div class="col-md-6">';
	echo '<span class="numberofpubs">'. $get_total_rows[0].'</span>';
	//echo '<a href="map.php">View Map</a>';
	echo '</div>';
	echo '<div class="col-md-6">';
	echo '<h3><a href="exportcsv.php">EXPORT CSV</a></h3>';
	echo '</div>';
	echo '</div>';
	//echo '<div id="map_canvas"></div>';
	while($results->fetch()){ //fetch values

	echo '<div class="col-md-4 pub-listing">
			<div class="panel panel-info">
				<div class="panel-heading">'.$id. '. <strong><a href="'.$Title.' '.$Surname.'" Title="'.$Surname.'">' .$Title.' '.$FirstName.' '.$Surname.'</a></strong></div>
				<div class="panel-body">
					<div class="list-group">
						<div class="list-group-item" style="background-color:#cdffd9;">
							<strong>Cotswold Plod:</strong> '.$miles.' miles<br />
							<strong>Drive time to Cotswold:</strong> '.$drive_time.'<br />
						</div>
						<div class="list-group-item" style="background-color:#ffe9cd;">
							<strong>South Downs Plod:</strong> '.$miles_sd.' miles<br />
							<strong>Drive time to South Downs:</strong> '.$drive_time_sd.'<br />
						</div>
					</div>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#map'.$id.'" data-whatever="@getbootstrap">more</button>
					<ul class="citylist">
					    <li><a href="#">'.$postcode.'</a></li>
					</ul>
					<div class="modal fade" id="map'.$id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				    <div class="modal-dialog" role="document">
				      <div class="modal-content">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				            <span aria-hidden="true">&times;</span>
				          </button>
				          <h4 class="modal-title" id="exampleModalLabel">' .$Title.' '.$FirstName.' '.$Surname.'</h4>
				        </div>
				        <div class="modal-body">
										' .$Title.' '.$FirstName.' '.$Surname.'
										<p>'.$Address1.'<br />'.$Address2.'<br />'.$Address3.'<br />'.$Town.'<br />'.$County.'<br />'.$postcode.'</p>
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>';
	}

	echo '</div>';

	echo '<div align="center">';
	/* We call the pagination function here to generate Pagination link for us.
	As you can see I have passed several parameters to the function. */
	echo paginate_function($item_per_page, $page_number, $get_total_rows[0], $total_pages);
	echo '</div>';

	exit;
}
################ pagination function #########################################
function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination">';

        $right_links    = $current_page + 3;
        $previous       = $current_page - 3; //previous link
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link

        if($current_page > 1){
			$previous_link = ($previous==0)? 1: $previous;
            $pagination .= '<li class="first"><a href="#" data-page="1" title="First">&laquo;</a></li>'; //first link
            $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous">&lt;</a></li>'; //previous link
                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
                    }
                }
            $first_link = false; //set first link to false
        }

        if($first_link){ //if current active page is first link
            $pagination .= '<li class="first active">'.$current_page.'</li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="last active">'.$current_page.'</li>';
        }else{ //regular current link
            $pagination .= '<li class="active">'.$current_page.'</li>';
        }

        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){
				$next_link = ($i > $total_pages) ? $total_pages : $i;
                $pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next">&gt;</a></li>'; //next link
                $pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
        }

        $pagination .= '</ul>';
    }
    return $pagination; //return pagination links
}

?>



<script type='text/javascript'>//<![CDATA[

var map = null;
var CustomMarker = null;
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();

function initialize() {
  $(".citylist").on("click", "li", function () {
    alert('clicked');
    getDirections($(this).text());
    });

  var myLatlng = new google.maps.LatLng(51.6869205,-2.3569287);
  var markerLatLng = new google.maps.LatLng(51.6869205,-2.3569287);
  var myOptions = {
      zoom: 13,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

  CustomMarker = new google.maps.Marker({
      position: markerLatLng,
      map: map,
      icon: "https://www.action.org.uk/sites/default/files/action-marker.png",
      animation: google.maps.Animation.DROP
  });
  directionsDisplay.setMap(map);
}



function getDirections(destination) {
  var start = CustomMarker.getPosition();
  var dest = destination;
  var request = {
      origin: start,
      destination: dest,
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function (result, status) {
      if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(result);
      }
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
//]]>

</script>
  </body>
</html>
