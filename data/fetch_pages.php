
<?php

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);

// Report all PHP errors (see changelog)
error_reporting(E_ALL);
//continue only if $_POST is set and it is a Ajax request
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

	include("config.inc.php");  //include config file
	//Get page number from Ajax POST
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
		$page_number = 1; //if there's no page number, set it to 1
	}

	//get total number of records from database for pagination
		$results = $mysqli->query("SELECT COUNT(*) FROM cotswolds");

	$get_total_rows = $results->fetch_row(); //hold total records in variable
	//break records into pages
	$total_pages = ceil($get_total_rows[0]/$item_per_page);

	//get starting position to fetch the records
	$page_position = (($page_number-1) * $item_per_page);



	//Limit our results within a specified range.

		$results = $mysqli->prepare("SELECT id, CompanyName, Address1, Address2, Locality, Town, County, postcode, Telephone, Title, FirstName, Surname, Position, Email, SICDescriptions, ModelledTurnover, NationalEmployees, WebAddress, drive_time, miles, lat, lon FROM data ORDER BY Surname ASC LIMIT $page_position, $item_per_page");
	$results->execute(); //Execute prepared Query

$results->bind_result($id, $CompanyName, $Address1, $Address2, $Locality, $Town, $County, $postcode, $Telephone, $Title, $FirstName, $Surname, $Position, $Email, $SICDescriptions, $ModelledTurnover, $NationalEmployees, $WebAddress, $drive_time, $miles, $lat, $lon ); //bind variables to prepared statement
	echo '</div>';
	//Display records fetched from database.
	echo '<div class="row">';
	echo '<div class="col-md-6">';
	echo '<span class="numberofpubs">'. $get_total_rows[0].'</span>';
	//echo '<a href="map.php">View Map</a>';
	echo '</div>';
	echo '<div class="col-md-6">';
	echo '<h3><a href="exportcsv.php">EXPORT CSV</a></h3>';
	echo '</div>';
	echo '</div>';
	//echo '<div id="map_canvas"></div>';
	while($results->fetch()){ //fetch values

	echo '<div class="col-md-4 pub-listing">
			<div class="panel panel-info">
				<div class="panel-heading">'.$id. '. <strong><a href="'.$Title.' '.$Surname.'" Title="'.$Surname.'">' .$Title.' '.$FirstName.' '.$Surname.'</a></strong></div>
				<div class="panel-body">
					<div class="list-group">
						<div class="list-group-item" style="background-color:#cdffd9;">
							<strong>Cotswold Plod:</strong> '.$miles.' miles<br />
							<strong>Drive time to Cotswold:</strong> '.$drive_time.'<br />
						</div>
						<input type="hidden" type="text" id="miles'.$id.'" value="'.$miles.'" name="milestocotswold" />
						<input type="hidden" type="text" id="drive'.$id.'" value="'.$drive_time.'" name="drivetocotswold" />
					</div>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#map'.$id.'" data-whatever="@getbootstrap">more</button>
					<ul class="citylist">
					    <li id="'.$id.'" name="' .$FirstName.' '.$Surname.'" value="'.$drive_time.'"><a href="#">'.$postcode.'</a></li>
					</ul>
					<div class="modal fade" id="map'.$id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				    <div class="modal-dialog" role="document">
				      <div class="modal-content">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				            <span aria-hidden="true">&times;</span>
				          </button>
				          <h4 class="modal-title" id="exampleModalLabel">' .$Title.' '.$FirstName.' '.$Surname.'</h4>
				        </div>
				        <div class="modal-body">
										' .$Title.' '.$FirstName.' '.$Surname.'
										<p>'.$Address1.'<br />'.$Address2.'<br />'.$Town.'<br />'.$County.'<br />'.$postcode.'</p>
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>';
	}

	echo '</div>';

	echo '<div align="center">';
	/* We call the pagination function here to generate Pagination link for us.
	As you can see I have passed several parameters to the function. */
	echo paginate_function($item_per_page, $page_number, $get_total_rows[0], $total_pages);
	echo '</div>';

	exit;
}
################ pagination function #########################################
function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination">';

        $right_links    = $current_page + 3;
        $previous       = $current_page - 3; //previous link
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link

        if($current_page > 1){
			$previous_link = ($previous==0)? 1: $previous;
            $pagination .= '<li class="first"><a href="#" data-page="1" title="First">&laquo;</a></li>'; //first link
            $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous">&lt;</a></li>'; //previous link
                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
                    }
                }
            $first_link = false; //set first link to false
        }

        if($first_link){ //if current active page is first link
            $pagination .= '<li class="first active">'.$current_page.'</li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="last active">'.$current_page.'</li>';
        }else{ //regular current link
            $pagination .= '<li class="active">'.$current_page.'</li>';
        }

        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){
				$next_link = ($i > $total_pages) ? $total_pages : $i;
                $pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next">&gt;</a></li>'; //next link
                $pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
        }

        $pagination .= '</ul>';
    }
    return $pagination; //return pagination links
}

?>
