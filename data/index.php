<?php include("config.inc.php");?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>This Admin - Data</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyA802YjrD55D2CSIzCO2PE5W1Pg8pUPwz0&ext=.js"></script>


    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">

    $(document).ready(function() {

    $("#results" ).load( "fetch_pages_sd.php?miles=2000"); //load initial records
    $("#towns" ).load( "fetch_towns.php"); //load initial records

    //executes code below when user click on pagination links
    $("#results").on( "click", ".pagination a", function (e){
    e.preventDefault();
    $(".loading-div").show(); //show loading element

    var page = $(this).attr("data-page"); //get page number from link
    $("#results").load("fetch_pages_sd.php",{"page":page}, function(){ //get content from PHP page
      $(".loading-div").hide(); //once done, hide loading element
    });
    });

    $('select[name="towns"]').change(function() {
    var town = $('select[name="towns"]').val();
    $("#results" ).load( "fetch_pages_sd.php?rsTown="+town);
    });
    });
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<style>
body,td,th {
	color: #333;
}
.contents{
	margin: 20px;
	padding: 20px;
	list-style: none;
	background: #F9F9F9;
	border: 1px solid #ddd;
	border-radius: 5px;
}
span.numberofpubs {
	font-size: 34px;
	text-align:right;
}
.pub-listing {
	min-height:300px;
}
.contents li{
    margin-bottom: 10px;
}
.loading-div{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.56);
	z-index: 999;
	display:none;
}
.loading-div img {
	margin-top: 20%;
	margin-left: 50%;
}

/* Pagination style */

.pagination li.active{
	    position: relative;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #DDDDDD;
    border: 1px solid #ddd;
}
.pagination>li {
    display: INLINE-FLEX;
}

.pointb { color:#669966; }

html, body, #map_canvas, #mapholder {
height: 400px;
width: 100%;
top:0px;
left:0px;
}

</style>
  </head>
  <body>
  	<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PLODS</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="index.php">Cotswolds </a></li>
              <li class="active"><a href="south-downs.php">South Downs <span class="sr-only">(current)</span></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      <div id="mapholder"></div>
    <div class="container">
	<?php
	//Limit our results within a specified range.
	//$results = $mysqli->query("SELECT DISTINCT event_name FROM cotswolds");
	//$theevent = $results->fetch_row();
 ?>
 <div class="row">
   <div class="col-md-6">
     <h1>Potential Registrants</h1>
   </div>
   <div class="col-md-6">
     <span class="pointa"></span>
     <h3><span class="pointb">
       Destination: <input id="orig" type="text" class="form-control">
    Origin: <input id="dest" type="text" class="form-control">
    Distance: <input id="dist" type="text" class="form-control">
    Time: <input id="time" type="text" class="form-control"></span></h3>

   </div>
 </div>
    <div id="second-choice">

    </div>
	  <div id="results"><!-- content will be loaded here --></div>

		</div>
    </div> <!-- /container -->
<div id="map_canvas"></div>

<script type="text/javascript">
$(document).change("#milesfromStart", function(){

    var milesfrom = $("#milesfromStart").val();
    $("#results").load("fetch_pages_sd.php?miles="+milesfrom);
    //alert(milesfrom);
});


$('#map_canvas').appendTo('#mapholder');
var map = null;
var CustomMarker = null;
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();

function initialize() {


  //getDirections($(this).text());
  //var theid = $(this).attr('id');
  //var person = $(this).attr('name') + ' ';
  //var drivetime = ' ('+$(this).attr('value')+')';
  //var milestext = " Miles ";
  //$(".pointb").text(person + $("#miles"+theid).val()+milestext + drivetime)


  var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };



  function error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
  };

  navigator.geolocation.getCurrentPosition(success, error, options);
      //document.getElementById('long').value = position.coords.longitude;
      //document.getElementById('lat').value = position.coords.latitude
      function success(pos) {
        var crd = pos.coords;
$(document).on("click", "ul.citylist li", function(){
        getDirections($(this).text())
        var origin = new google.maps.LatLng(crd.latitude,crd.longitude),
            destination = $(this).text(),
            service = new google.maps.DistanceMatrixService();

        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                avoidHighways: false,
                avoidTolls: false
            },
            callback
        );
      });
        console.log('Your current position is:');
        console.log('Latitude : ' + crd.latitude);
        console.log('Longitude: ' + crd.longitude);
        console.log('More or less ' + crd.accuracy + ' meters.');

//alert('Latitude: '+crd.latitude);
      var myLatlng = new google.maps.LatLng(crd.latitude,crd.longitude);
      var markerLatLng = new google.maps.LatLng(crd.latitude,crd.longitude);


var myOptions = {
    zoom: 13,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
};
var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

CustomMarker = new google.maps.Marker({
    position: markerLatLng,
    map: map,
    icon: "https://www.action.org.uk/sites/default/files/action-marker.png",
    animation: google.maps.Animation.DROP
});
directionsDisplay.setMap(map);
}



function getDirections(destination) {
//var start = CustomMarker.getPosition();
var start = CustomMarker.getPosition();
var dest = destination;
var request = {
    origin: start,
    destination: dest,
    travelMode: google.maps.TravelMode.DRIVING
};
directionsService.route(request, function (result, status) {
    if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(result);
    }
});
};
}


function callback(response, status) {
    var orig = document.getElementById("orig"),
        dest = document.getElementById("dest"),
        dist = document.getElementById("dist");

    if(status=="OK") {
        orig.value = response.destinationAddresses[0];
        dest.value = response.originAddresses[0];
        dist.value = response.rows[0].elements[0].distance.text;
        time.value = response.rows[0].elements[0].duration.text;
    } else {
        alert("Error: " + status);
    }
}
google.maps.event.addDomListener(window, 'load', initialize);
//]]>

</script>


<!--<ul class="citylist">
  <li><a href="#">BN14 7HX</a></li>
  <li><a href="#">RH12 2DP</a></li>
  <li><a href="#">BN15 8AR</a></li>
  <li><a href="#">Harrogate</a></li>
</ul>-->
  </body>
</html>
